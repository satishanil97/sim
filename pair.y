%{
  #include<stdio.h>
  #include<stdlib.h>
%}

%token DIGIT NEWLINE

%%

start : pair NEWLINE  {
                        printf("\nComplete\n");
                        exit(1);
                      }
pair : num ',' num    {
                        printf("pair(%d , %d)\n",$1,$3);
                      }
num : DIGIT           {$$ = $1;}

%%

yyerror(char const *s)
{
  printf("yerror %s",s);
}

int main()
{
  yyparse();
  return 1;
}
