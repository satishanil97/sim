%{

  #include<stdio.h>
  #include<string.h>
  #include<stdlib.h>
  #define YYSTYPE char*

  %}

  %token STRING
  %token NEWLINE
  %left '+'

  %%

  start : expr NEWLINE           {
                                    printf("\nConcatenated string is %s\n",$1);
                                    exit(1);
                                  }
	        ;

  expr:  expr '+' expr        {
                                strcat($1,$3);
                                strcpy($$,$1);
                              }
	       | STRING            {strcpy($$,$1);}
	      ;

  %%

  yyerror(char const *s)
  {
      printf("yyerror  %s\n",s);
  }
  int main()
  {
	  yyparse();
	  return 1;
  }
