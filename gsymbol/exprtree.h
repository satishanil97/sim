struct tnode{
	int type;			//type of node	1 for ID(leaf) , 2 for operator node , 3 for cond node , 0 undeclared
	int val;			//value of for the expression tree
	char op;			//indicates the opertor branch
	char id;			//id name
	struct tnode *ptr1,*ptr2,*ptr3;  	//left and right branches
}tnode;

/*Make a leaf tnode and set the value of val field*/
struct tnode* makeLeafNode(int n, char name);

/*Make a tnode with opertor, left and right branches set*/
struct tnode* makeOperatorNode(char c,struct tnode *l,struct tnode *r);

/*Make a conditional expression node*/
struct tnode* makeCondNode(struct tnode *l, struct tnode *m, struct tnode *r);

/*To evaluate an expression tree*/
int evaluate(struct tnode *t);
