struct Gsymbol {
  char *NAME; // Name of the Identifier
  int TYPE; // TYPE can be INTEGER or BOOLEAN
  int SIZE; // Size field for arrays
  int BINDING; // Address of the Identifier in Memory
  struct Gsymbol *NEXT; //Pointer to next symbol table entry
};

struct Gsymbol *HEAD;

//Creates a Global symbol table entry with the given parameters
struct Gsymbol* GInstall(char *name, int type, int size);

//Search for a GST entry with the given 'name', if exists, return pointer to GST entry else return NULL.
struct Gsymbol* GLookup(char *name);
