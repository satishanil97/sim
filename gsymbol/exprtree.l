%{
	#include <stdio.h>
	#include "y.tab.h"
	#include <stdlib.h>

	int number;

%}

%%

[0-9]+	{number = atoi(yytext); yylval.node = makeLeafNode(number,'N'); return NUM;}
[a-z]		{yylval.node = makeLeafNode(0,yytext[0]); return ID;}
"+"	{return PLUS;}
"-"	{return MINUS;}
"*"	{return MUL;}
"/"	{return DIV;}
"if"	{return IF;}
"then"	{return THEN;}
"else"	{return ELSE;}
"endif"	{return ENDIF;}
"write" {return WRITE;}
"while" {return WHILE;}
"endwhile" {return ENDWHILE;}
"do" {return DO;}
"break" {return BREAK;}
"continue" {return CONTINUE;}
"<"			{return LT;}
">"			{return GT;}
"="			{return ASGN;}
[;]	{return *yytext;}
[ \t]	{}
[()]	{return *yytext;}
[\n]	{return END;}
.	{printf("\n%s\n",yytext);yyerror("unknown character\n");exit(1);}

%%

int yywrap(void) {
	return 1;
}
