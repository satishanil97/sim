#include<stdio.h>
#include<string.h>

struct Gsymbol* GInstall(char *name, int type, int size){

  struct Gsymbol *temp;

  temp = (struct Gsymbol*)malloc(sizeof(struct Gsymbol));
  temp->NAME = strdup(name);
  temp->TYPE = type;
  temp->SIZE = size;
  temp->BINDING = 0;
  temp->NEXT = HEAD;

  HEAD = temp;

  return temp;
}



struct Gsymbol* GLookup(char *name){

  struct Gsymbol *temp;

  temp = HEAD;

  while(temp != NULL)
  {
    if(!strcmp(temp->NAME , name))
      break;

    temp = temp->NEXT;
  }

  return temp;
}
