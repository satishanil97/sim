%{
	#include<stdio.h>
	#include "y.tab.h"
	#include<stdlib.h>
	#include<string.h>

	int number;
%}

%%

"integer" {return INTEGER;}
"decl" {return DECL;}
"enddecl" {return ENDDECL;}
[a-zA-Z][a-zA-Z0-9]* {yylval.string = strdup(yytext); return ID;}
"+" {return PLUS;}
"-" {return MINUS;}
"*" {return MUL;}
"/" {return DIV;}
[\t] {}
" " {}
[()] {return *yytext;}
[\n] {return END;}
[;] {return *yytext;}
[,] {return *yytext;}
.    {yyerror("unknown character:\n"); exit(1);}

%%

int yywrap(void) {
	return 1;
}
