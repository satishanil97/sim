%{
	#include<stdio.h>
	#include<stdlib.h>


	#include "gsymbol.h"
	#include "gsymbol.c"
	int yylex(void);
%}

%union {
	int val;
	struct tnode *nptr;
	char * string;
}

%token <string> ID
%token <nptr> expr
%token INTEGER DECL ENDDECL
%token NUM PLUS MINUS DIV END
%left PLUS MINUS
%left MUL DIV

%%

declarations : DECL END declist END ENDDECL END {

					printf("Declarations Complete\n");
	    		struct Gsymbol *temp = HEAD;

					while(temp != NULL) {
						printf("%s, ", temp->NAME);
						temp = temp->NEXT;
					}
					printf("\n");
					exit(1);
				}
	     ;

declist : decl
	 | decl declist
	 ;

decl : INTEGER idlist ';'
     ;

idlist : ID ',' idlist {GInstall($1, 1, 4);}
       | ID {GInstall($1, 1, 4);}

 /*program : expr END {
			$$ = $2;
			printf("Answer: %d\n",evaluate($1));
			exit(1);
		   }
	;

expr : expr PLUS expr {$$ = makeOperatorNode('+', $1, $3);}
     | expr MINUS expr{$$ = makeOperatorNode('-', $1, $3);}
     | expr MUL expr {$$ = makeOperatorNode('*', $1, $3);}
     | expr DIV expr {$$ = makeOperatorNode('/', $1, $3);}
     | '(' expr ')' {$$ = $2;}
     | NUM {$$ = $1;}
     ; */

%%

yyerror(char const *s) {
	printf("yyerror: %s",s);
}

int main(void) {
	yyparse();
	return 0;
}
