%{
	#include <stdio.h>
	#include <stdlib.h>
	#include "exprtree.h"
	#include "exprtree.c"

	int yylex(void);
%}

%union{
	struct tnode* node;
	int integer;
	char character;
}

%type <node> expr
%type <node> stmt
%type <node> slist
%type <node> Program
%type <node> NUM
%type <node> ID
%token ID WRITE ASGN READ
%token IF THEN ELSE ENDIF WHILE DO ENDWHILE BREAK CONTINUE
%token NUM END
%left PLUS MINUS LT GT
%left MUL DIV

%%

Program : slist END	{
	$$ = $1;
	evaluate($$);
	exit(1);
}
        ;

slist : slist stmt	{
	$$ = makeOperatorNode('S',$1,$2);
}
      | stmt	{
				$$ = $1;
			}
      ;

stmt : ID ASGN expr ';'      {
																$$ = makeOperatorNode('=',$1,$3);
                             }

      | READ '(' ID ')' ';'  	{
                                $$ = makeOperatorNode('R',$3,NULL);
                              }

      | WRITE '(' ID ')' ';' 	{
																	$$ = makeOperatorNode('W',$3,NULL);
                            	}

			|	IF expr THEN slist ENDIF ';'	{
				$$ = makeCondNode($2,$4,NULL);
			}

			| IF expr THEN slist ELSE slist ENDIF ';'	{
				$$ = makeCondNode($2,$4,$6);
			}
			;


expr : expr PLUS expr		{$$ = makeOperatorNode('+',$1,$3);}
			| expr MINUS expr  	{$$ = makeOperatorNode('-',$1,$3);}
			| expr MUL expr	{$$ = makeOperatorNode('*',$1,$3);}
			| expr DIV expr	{$$ = makeOperatorNode('/',$1,$3);}
			| expr LT expr	{$$ = makeOperatorNode('<',$1,$3);}
			| expr GT expr 	{$$ = makeOperatorNode('>',$1,$3);}
			| '(' expr ')'		{$$ = $2;}
			| NUM			{$$ = $1;}
			| ID {
	            $$ = $1;
	        }
			;

/*
expr : expr PLUS expr  {$$ = $1 + $3;}
    | expr MINUS expr   {$$ = $1 - $3;}
    | expr MUL expr   {$$ = $1 * $3;}
    | expr DIV expr   {$$ = $1 / $3;}
    | '(' expr ')'    {$$ = $2;}
    | NUM           {$$ = $1;}
    | ID {
            if(var[$1 - 'a'] == NULL)
              printf("unassigned variable");

            else
              $$ = *var[$1 - 'a'];
          }
    ;
*/

/*
program : expr END	{
				$$ = $2;
				printf("Answer : %d\n",evaluate($1));
				exit(1);
			}
		;

expr : expr PLUS expr		{$$ = makeOperatorNode('+',$1,$3);}
	 | expr MINUS expr  	{$$ = makeOperatorNode('-',$1,$3);}
	 | expr MUL expr	{$$ = makeOperatorNode('*',$1,$3);}
	 | expr DIV expr	{$$ = makeOperatorNode('/',$1,$3);}
	 | '(' expr ')'		{$$ = $2;}
	 | NUM			{$$ = $1;}
	 ;
*/
%%

yyerror(char const *s)
{
    printf("yyerror %s",s);
}


int main(void) {
	yyparse();
	return 0;
}
