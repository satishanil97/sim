#include<stdio.h>
#include<stdlib.h>

struct tnode* makeLeafNode(int n, char name){
    struct tnode *temp;
    temp = (struct tnode*)malloc(sizeof(struct tnode));
    temp->op = 'X';
    temp->id = name;
    temp->val = n;
    temp->ptr1 = NULL;
    temp->ptr2 = NULL;
    temp->ptr3 = NULL;

    if((name >= 'a' && name <= 'z') || (name == 'N'))
      temp->type = 1;

    else
      temp->type = 0;

    return temp;
}

struct tnode* makeOperatorNode(char c,struct tnode *l,struct tnode *r){
    struct tnode *temp;
    temp = (struct tnode*)malloc(sizeof(struct tnode));
    temp->op = c;
    temp->ptr1 = l;
    temp->ptr2 = r;
    temp->ptr3 = NULL;
    temp->type = 2;

    return temp;
}

struct tnode* makeCondNode(struct tnode *l, struct tnode *m, struct tnode *r){
    struct tnode *temp;
    temp = (struct tnode*)malloc(sizeof(struct tnode));
    temp->type = 3;
    temp->ptr1 = l;
    temp->ptr2 = m;
    temp->ptr3 = r;
    return temp;
}

int evaluate(struct tnode *t){

    if(t == NULL)
      return 0;

    if(t->type == 1){
      return t->val;
    }

    else if (t->type == 2){

        switch(t->op){
            case '+' : return evaluate(t->ptr1) + evaluate(t->ptr2);
                       break;
            case '-' : return evaluate(t->ptr1) - evaluate(t->ptr2);
                       break;
            case '*' : return evaluate(t->ptr1) * evaluate(t->ptr2);
                       break;
            case '/' : return evaluate(t->ptr1) / evaluate(t->ptr2);
                       break;
            case '<' : return evaluate(t->ptr1) < evaluate(t->ptr2);
                       break;
            /*case '<=': return evaluate(t->ptr1) <= evaluate(t->right);
                       break;
            case '==': return evaluate(t->ptr1) == evaluate(t->right);
                       break;*/
            case '>' : return evaluate(t->ptr1) > evaluate(t->ptr2);
                       break;
            /*case '>=': return evaluate(t->ptr1) >= evaluate(t->right);
                       break;
            case '!=': return evaluate(t->ptr1) != evaluate(t->right);
                       break;
            case '&&': return evaluate(t->ptr1) && evaluate(t->right);
                       break;
            case '||': return evaluate(t->ptr1) || evaluate(t->right);
                       break;*/
            case 'S' :
              evaluate(t->ptr1);
              if(t->ptr2 != NULL){
                evaluate(t->ptr2);
              }
              break;

            case 'W' :
              printf("%d\n",evaluate(t->ptr1));
              break;

            case 'R' :
              printf("\nEnter value : ");
              scanf("%d",&(t->ptr1->val));

            case '='  :
              (t->ptr1)->val = evaluate(t->ptr2);
              break;

        }

    }

    else if(t->type == 3){
      if(evaluate(t->ptr1)){
        evaluate(t->ptr2);
      }

      else if(t->ptr3 != NULL){
        evaluate(t->ptr3);
      }
    }

    else{
      return 0;
    }
}
