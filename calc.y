%{

  #include<stdio.h>
  #include<stdlib.h>

  %}

  %token DIGIT NEWLINE
  %left '+' '-'
  %left '*' '/'

  %%

  start : expr NEWLINE           {
                                    printf("\nValue of Expression is %d\n",$1);
                                    exit(1);
                                  }
	        ;

  expr:  expr '+' expr        {$$ = $1 + $3;}
	    | expr '-' expr     {$$ = $1 - $3;}
      | expr '*' expr     {$$ = $1 * $3;}
      | expr '/' expr     {$$ = $1 / $3;}
	    | '(' expr ')'      {$$ = $2;}
	    | DIGIT             {$$ = $1;}
	    ;

  %%

  yyerror(char const *s)
  {
      printf("yyerror  %s\n",s);
  }
  int main()
  {
	  yyparse();
	  return 1;
  }
