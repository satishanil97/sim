#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//      CREATE NODE FUNCTION
struct tnode *createNode(struct Typetable *type, int nodetype, int val, char *name, struct tnode *ptr1, struct tnode *ptr2, struct tnode *ptr3, struct exprlist *arglist){
  struct tnode *t,*tree;
  struct Gsymbol *gptr;
  struct Argument *temp;
  struct exprlist *ptr;
  struct Typetable *type1,*type2;

  t = (struct tnode*)malloc(sizeof(struct tnode));
  t->type = type;
  t->nodetype = nodetype;
  t->val = val;

  if(name != NULL)
    t->name = strdup(name);

  t->ptr1 = ptr1;
  t->ptr2 = ptr2;
  t->ptr3 = ptr3;

  t->arglist = arglist;
  if(name != NULL)
  {
    t->lentry = LLookup(name);
    t->gentry = GLookup(name);
  }

  if((t->lentry == NULL)&&(t->gentry == NULL)&&(t->nodetype != 24)&&(name != NULL))
  {
    printf("undeclared variable %s\n",name);
    exit(1);
  }


  switch(t->nodetype){
      case 0 :                          // ID
                if((t->lentry != NULL) && (t->ptr1 != NULL)){
                  printf("Local variable is not an array\n");
                  exit(1);
                }
                else if(t->ptr1 != NULL){
                  if(strcmp(t->ptr1->type->name,"integer")){
                    printf("Array index should be integer\n");
                    exit(1);
                  }
                }

      case 1 :    //NUM
        break;

      case 2 :  // PLUS
               if(t->ptr1->nodetype == 24){
                 tree = t->ptr1;
                 type1 = tree->type;

                 while (tree != NULL) {
                   type1 = tree->type;
                   tree = tree->ptr1;
                 }

                 if(strcmp(type1->name,"integer")){
                   printf("Type Mismatch for + \n");
                   exit(1);
                 }
               }

               if(t->ptr2->nodetype == 24){
                 tree = t->ptr2;
                 type1 = tree->type;

                 while (tree != NULL) {
                   type1 = tree->type;
                   tree = tree->ptr1;
                 }

                 if(strcmp(type->name,"integer")){
                   printf("Type Mismatch for + \n");
                   exit(1);
                 }
               }

               if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){
                 if((strcmp(t->ptr1->type->name,"integer")) || (strcmp(t->ptr2->type->name,"integer"))){
                   printf("Type Mismatch for -\n");
                   exit(1);
                 }
               }

               break;

      case 3 : //MINUS
                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for - \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for - \n");
                    exit(1);
                  }
                }
               if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){
                 if((strcmp(t->ptr1->type->name,"integer")) || (strcmp(t->ptr2->type->name,"integer"))){  // MINUS
                   printf("Type Mismatch for -\n");
                   exit(1);
                 }
               }

               break;

      case 4 : //MUL
                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for * \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for * \n");
                    exit(1);
                  }
                }

                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){
                  if((strcmp(t->ptr1->type->name,"integer")) || (strcmp(t->ptr2->type->name,"integer"))){  // MUL
                    printf("Type Mismatch for *\n");
                    exit(1);
                  }
                }

               break;

      case 5 :  //DIV
                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for * \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for * \n");
                    exit(1);
                  }
                }

                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){
                  if((strcmp(t->ptr1->type->name,"integer")) || (strcmp(t->ptr2->type->name,"integer"))){  // DIV
                    printf("Type Mismatch for /\n");
                    exit(1);
                  }
                }

               break;

      case 6 : //LT
                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){   //LT
                  if((strcmp(t->ptr1->type->name,t->ptr2->type->name)) || !strcmp(t->ptr1->type->name,"boolean") || !strcmp(t->ptr2->type->name,"boolean")){
                    printf("Type Mismatch for <\n");
                    exit(1);
                  }
                }

                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for < \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for < \n");
                    exit(1);
                  }
                }

                break;

      case 7 :  //LE

              if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){   //LE
                if((strcmp(t->ptr1->type->name,t->ptr2->type->name)) || !strcmp(t->ptr1->type->name,"boolean") || !strcmp(t->ptr2->type->name,"boolean")){
                  printf("Type Mismatch for <=\n");
                  exit(1);
                }
              }

              if(t->ptr1->nodetype == 24){
                tree = t->ptr1;
                type1 = tree->type;

                while (tree != NULL) {
                  type1 = tree->type;
                  tree = tree->ptr1;
                }

                if(strcmp(type1->name,"integer")){
                  printf("Type Mismatch for <= \n");
                  exit(1);
                }
              }

              if(t->ptr2->nodetype == 24){
                tree = t->ptr2;
                type1 = tree->type;

                while (tree != NULL) {
                  type1 = tree->type;
                  tree = tree->ptr1;
                }

                if(strcmp(type1->name,"integer")){
                  printf("Type Mismatch for <= \n");
                  exit(1);
                }
              }

               break;

      case 8 : //EQ

                if((t->ptr1->nodetype != 27) && (t->ptr2->nodetype != 27)){   //EQ
                  type1 = t->ptr1->type;
                  if(t->ptr1->nodetype == 24){
                    tree = t->ptr1;
                    type1 = tree->type;

                    while (tree != NULL) {
                      type1 = tree->type;
                      tree = tree->ptr1;
                    }

                  }

                  type2 = t->ptr2->type;
                  if(t->ptr2->nodetype == 24){
                    tree = t->ptr2;
                    type2 = tree->type;

                    while (tree != NULL) {
                      type2 = tree->type;
                      tree = tree->ptr1;
                    }

                  }

                  if(strcmp(type1->name,type2->name)){
                    printf("Type Mismatch in ==\n");
                  }
                }


               break;

      case 9 : //GE
                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){   //GE
                  if((strcmp(t->ptr1->type->name,t->ptr2->type->name)) || !strcmp(t->ptr1->type->name,"boolean") || !strcmp(t->ptr2->type->name,"boolean")){
                    printf("Type Mismatch for >=\n");
                    exit(1);
                  }
                }

                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for >= \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for >= \n");
                    exit(1);
                  }
                }

               break;

      case 10 : //GT

                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){   //GT
                  if((strcmp(t->ptr1->type->name,t->ptr2->type->name)) || !strcmp(t->ptr1->type->name,"boolean") || !strcmp(t->ptr2->type->name,"boolean")){
                    printf("Type Mismatch for >\n");
                    exit(1);
                  }
                }

                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for > \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"integer")){
                    printf("Type Mismatch for > \n");
                    exit(1);
                  }
                }

                break;

      case 11 : //NE

                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){   //NE
                  type1 = t->ptr1->type;
                  if(t->ptr1->nodetype == 24){
                    tree = t->ptr1;
                    type1 = tree->type;

                    while (tree != NULL) {
                      type1 = tree->type;
                      tree = tree->ptr1;
                    }

                  }

                  type2 = t->ptr2->type;
                  if(t->ptr2->nodetype == 24){
                    tree = t->ptr2;
                    type2 = tree->type;

                    while (tree != NULL) {
                      type2 = tree->type;
                      tree = tree->ptr1;
                    }

                  }

                  if(strcmp(type1->name,type2->name)){
                    printf("Type Mismatch in !=\n");
                  }
                }


                break;

      case 12 : //AND
                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){
                  if((strcmp(t->ptr1->type->name,"boolean")) || (strcmp(t->ptr2->type->name,"boolean"))){  // AND
                    printf("Type Mismatch for AND\n");
                    exit(1);
                  }
                }

                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"boolean")){
                    printf("Type Mismatch for AND \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"boolean")){
                    printf("Type Mismatch for AND \n");
                    exit(1);
                  }
                }

                break;

      case 13 : //OR
                if((t->ptr1->nodetype != 24) && (t->ptr2->nodetype != 24)){     // OR
                  if((strcmp(t->ptr1->type->name,"boolean")) || (strcmp(t->ptr2->type->name,"boolean"))){
                    printf("Type Mismatch for OR\n");
                    exit(1);
                  }
                }

                if(t->ptr1->nodetype == 24){
                  tree = t->ptr1;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"boolean")){
                    printf("Type Mismatch for AND \n");
                    exit(1);
                  }
                }

                if(t->ptr2->nodetype == 24){
                  tree = t->ptr2;
                  type1 = tree->type;

                  while (tree != NULL) {
                    type1 = tree->type;
                    tree = tree->ptr1;
                  }

                  if(strcmp(type1->name,"boolean")){
                    printf("Type Mismatch for AND \n");
                    exit(1);
                  }
                }

                break;

      case 14 :     //statement
        if(strcmp(t->ptr1->type->name, "void")){
          printf("Type Mismatch - statement must be void\n");
          exit(1);
        }

        if(t->ptr2 != NULL){
          if(strcmp(t->ptr2->type->name, "void")){
            printf("Type Mismatch - statement must be void\n");
            exit(1);
          }
        }
        break;

      case 16 :     //WRITE
        if(!strcmp(t->ptr1->type->name, "void")){
          printf("Wrong Type for write\n");
          exit(1);
        }

        break;

      case 15 :     //READ
        if(!strcmp(t->ptr1->type->name, "void")){
          printf("Wrong Type for read\n");
          exit(1);
        }
        break;

      case 17  :    //assignment stmt
        if((!strcmp(t->ptr1->type->name, "void")) || (!strcmp(t->ptr2->type->name, "void"))){
          printf("Illegal assignment\n");
          exit(1);
        }

        type1 = t->ptr1->type;
        type2 = t->ptr2->type;

        if(t->ptr1->nodetype == 24){
          tree = t->ptr1;
          type1 = tree->type;

          while (tree != NULL) {
            type1 = tree->type;
            tree = tree->ptr1;
          }

        }

        if(t->ptr2->nodetype == 24){
          tree = t->ptr2;
          type2 = tree->type;

          while (tree != NULL) {
            type2 = tree->type;
            tree = tree->ptr1;
          }

        }

        else if(strcmp(type1->name,type2->name) && strcmp(type2->name,"null")) {
          printf("Illegal assignment - type mismatch\n");
          exit(1);
        }

        break;

      case 18 :     //conditional expression node
        type1 = t->ptr1->type;
        if(t->ptr1->nodetype == 24){
          tree = t->ptr1;
          type1 = tree->type;

          while (tree != NULL) {
            type1 = tree->type;
            tree = tree->ptr1;
          }

        }

        if((strcmp(type1->name,"boolean"))||(strcmp(t->ptr2->type->name, "void"))){
          printf("Wrong Type for conditional check\n");
          exit(1);
        }

        else if(t->ptr3 != NULL){
          if(strcmp(t->ptr3->type->name, "void")){
            printf("Wrong Type - statement must be of type - void\n");
            exit(1);
          }

        }
        break;

      case 19 :   //while
        type1 = t->ptr1->type;
        if(t->ptr1->nodetype == 24){
          tree = t->ptr1;
          type1 = tree->type;

          while (tree != NULL) {
            type1 = tree->type;
            tree = tree->ptr1;
          }

        }

        if((strcmp(type1->name,"boolean"))||(strcmp(t->ptr2->type->name, "void"))){
          printf("Wrong Type for condition in while\n");
          exit(1);
        }

        break;

      case 20 : //break
        break;

      case 21 : //continue
        break;

      case 22 : //function body
        break;

      case 23 : //function
        gptr = GLookup(t->name);
        temp = gptr->args;
        ptr = t->arglist;

        while((temp != NULL)&&(ptr != NULL)){
          if(strcmp(temp->type->name , ptr->t->type->name)){
            printf("Argument type mismatch for function call of %s\n",name);
            exit(1);
          }

          temp = temp->next;
          ptr = ptr->next;
        }

        if((ptr != NULL)||(temp != NULL)){
          printf("Mismatch in no of arguments to function call of %s\n",name);
          exit(1);
        }
        break;

      case 24 : //field
        break;

      case 25 : //free
        break;

      case 26 : //alloc
        break;

      case 27: //null
        break;

      default : return NULL;

  }

  return t;
}

//    FUNCTION TO GET A FREE REGISTER
int get_reg(){
  if(regno == 19){
    return -1;
  }

  else{
    regno++;
    return regno;
  }
}

//    FUNCTION TO FREE A REGISTER
int free_reg(){
  if(regno == -1){
    return -1;
  }

  else{
    regno--;
    return 1;
  }
}

//FUNCTION TO GENERATE HEADER
void headerGen(FILE *targetfile){
  int loc,r0,r1;
  fprintf(targetfile, "0\n2056\n0\n0\n0\n0\n0\n0\n");
  loc = 4096 + memory;
  ip = 2056;
  base = loc;
  fprintf(targetfile,"MOV SP, %d\n",loc);
  ip += 2;
  fprintf(targetfile, "BRKP\n");
  ip += 2;
  fprintf(targetfile,"MOV BP, %d\n",loc);
  ip += 2;

  r0 = get_reg();
  r1 = get_reg();
  fprintf(targetfile, "MOV R%d, \"Heapset\"\n",r1); //function name
  ip += 2;
  fprintf(targetfile, "PUSH R%d\n",r1);
  ip += 2;
  fprintf(targetfile, "PUSH R%d\n",r1); //argument 1
  ip += 2;
  fprintf(targetfile, "PUSH R%d\n",r1); //argument 2
  ip += 2;
  fprintf(targetfile, "PUSH R%d\n",r1); //argument 3
  ip += 2;
  fprintf(targetfile, "PUSH R%d\n",r1); //space for return value
  ip += 2;
  fprintf(targetfile, "BRKP\n");
  ip += 2;
  fprintf(targetfile, "CALL 0\n"); //call to library interface
  ip += 2;
  fprintf(targetfile, "POP R%d\n",r0); //get the return value to r0
  ip += 2;
  fprintf(targetfile, "POP R%d\n",r1); //argument 3
  ip += 2;
  fprintf(targetfile, "POP R%d\n",r1); //argument 2
  ip += 2;
  fprintf(targetfile, "POP R%d\n",r1); //argument 1
  ip += 2;
  fprintf(targetfile, "POP R%d\n",r1); //function name
  ip += 2;
  free_reg();
  free_reg();
  fprintf(targetfile, "BRKP\n");
  ip += 2;

  fprintf(targetfile, "CALL F0\n");
  ip += 2;
  labelnum = 0;
  top = 0;
}


//FUNCTION TO GENERATE XSM CODE
int codeGen(struct tnode *t , FILE *targetfile){
    int r,r0,r1,loc,label0,label1,n,lsize;
    struct Fieldlist *fentry;
    struct Typetable *type;
    int rcount = 0,retspace;


    if(t == NULL)
      return 0;

    switch(t->nodetype){

        case 0 :    // IDENTIFIER

                 if(t->lentry != NULL) { //local variable or argument
             			if(t->lentry->isarg == 0) { //local variable
             				r0 = get_reg();
             				fprintf(targetfile, "MOV R%d, BP\n",r0);
             				ip += 2;
             				fprintf(targetfile, "ADD R%d, %d\n",r0,t->lentry->binding+1);
             				ip += 2;
             				fprintf(targetfile, "MOV R%d, [R%d]\n",r0,r0);
             				ip += 2;
             				return r0;
             			}
             			else if(t->lentry->isarg == 1) { //argument
             				r0 = get_reg();
             				fprintf(targetfile, "MOV R%d, BP\n",r0);
             				ip += 2;
             				fprintf(targetfile, "SUB R%d, %d\n",r0,2+numarg);
             				ip += 2;
             				fprintf(targetfile, "ADD R%d, %d\n",r0,t->lentry->binding);
             				ip += 2;
             				fprintf(targetfile, "MOV R%d, [R%d]\n",r0,r0);
             				ip += 2;
             				return r0;
             			}
             		}


                else if(t->ptr1 == NULL){ //global variable
                   r = get_reg();
                   loc = t->gentry->binding;
                   fprintf(targetfile, "MOV R%d, [%d]\n",r,loc);
                   ip += 2;
                 }

                 else{
                   r = get_reg();
                   r0 = codeGen(t->ptr1 , targetfile);
                   r1 = get_reg();
                   loc = t->gentry->binding;
                   fprintf(targetfile, "MOV R%d, %d\n",r1,loc);
                   ip += 2;
                   fprintf(targetfile, "ADD R%d, R%d\n",r1,r0);
                   ip += 2;
                   fprintf(targetfile, "MOV R%d, [R%d]\n",r,r1);
                   ip += 2;
                   free_reg();
                   free_reg();
                 }
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;

                 return r;
                 break;

        case 1 : // NUMBER

                 r = get_reg();
                 n = t->val;
                 fprintf(targetfile, "MOV R%d, %d\n",r,n);
                 ip += 2;
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r;
                 break;

        case 2 : //  PLUS

                  r0 = codeGen(t->ptr1, targetfile);
                  r1 = codeGen(t->ptr2, targetfile);
                  fprintf(targetfile, "ADD R%d, R%d\n",r0,r1);
                  ip += 2;
                  free_reg();
                  fprintf(targetfile, "BRKP\n");
                  ip += 2;
                  return r0;
                  break;

        case 3 :  //  MINUS
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "SUB R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 4 : //  MUL
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "MUL R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 5 : //  DIV
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "DIV R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 6 : //  LT
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "LT R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 7 : //  LE
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "LE R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 8 : //  EQ
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "EQ R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 9 : //  GE
                 r0 = codeGen(t->ptr1, targetfile);
                 r1 = codeGen(t->ptr2, targetfile);
                 fprintf(targetfile, "GE R%d, R%d\n",r0,r1);
                 ip += 2;
                 free_reg();
                 fprintf(targetfile, "BRKP\n");
                 ip += 2;
                 return r0;
                 break;

        case 10 : //  GT
                  r0 = codeGen(t->ptr1, targetfile);
                  r1 = codeGen(t->ptr2, targetfile);
                  fprintf(targetfile, "GT R%d, R%d\n",r0,r1);
                  ip += 2;
                  free_reg();
                  fprintf(targetfile, "BRKP\n");
                  ip += 2;
                  return r0;
                  break;

        case 11 : //  NE
                  r0 = codeGen(t->ptr1, targetfile);
                  r1 = codeGen(t->ptr2, targetfile);
                  fprintf(targetfile, "NE R%d, R%d\n",r0,r1);
                  ip += 2;
                  free_reg();
                  fprintf(targetfile, "BRKP\n");
                  ip += 2;
                  return r0;
                  break;

        case 12 :
                  /*return evaluate(t->ptr1) && evaluate(t->ptr2);  //  AND
                  r0 = codeGen(t->ptr1, targetfile);
                  r1 = codeGen(t->ptr2, targetfile);
                  fprintf(targetfile, "NE R%d, R%d\n",r0,r1);
                  free_reg();
                  return r0;*/
                  break;

        case 13 :
                  //return evaluate(t->ptr1) || evaluate(t->ptr2);  //  OR
                  break;

        case 14 :     //statement

                  codeGen(t->ptr1, targetfile);
                  if(t->ptr2 != NULL) {
                     codeGen(t->ptr2, targetfile);
                  }
                  return -1;

                  break;

        case 15 :     //READ

          /*r = get_reg();

          if(t->ptr1->lentry != NULL) { //local variable or argument
            if(t->ptr1->lentry->isarg == 0) { //local variable
              fprintf(targetfile, "MOV R%d, BP\n",r);
              ip += 2;
              fprintf(targetfile, "ADD R%d, %d\n",r,t->ptr1->lentry->binding+1);
              ip += 2;
            }
            else if(t->ptr1->lentry->isarg == 1) { //argument
              fprintf(targetfile, "MOV R%d, BP\n",r);
              ip += 2;
              fprintf(targetfile, "SUB R%d, %d\n",r,2+numarg);
              ip += 2;
              fprintf(targetfile, "ADD R%d, %d\n",r,t->ptr1->lentry->binding);
              ip += 2;
            }
          }

          else if(t->ptr1->ptr1 == NULL){
            loc = t->ptr1->gentry->binding;
            fprintf(targetfile, "MOV R%d, %d\n",r,loc);
            ip += 2;
          }

          else{
            r0 = codeGen(t->ptr1->ptr1 , targetfile);
            loc = t->ptr1->gentry->binding;
            fprintf(targetfile, "MOV R%d, %d\n",r,loc);
            ip += 2;
            fprintf(targetfile, "ADD R%d, R%d\n",r,r0);
            ip += 2;
            free_reg();
          }*/
          type = t->ptr1->type;
          r = get_reg();

          if(t->ptr1->lentry != NULL) { //local variable or argument
           if(t->ptr1->lentry->isarg == 0) { //local variable
             fprintf(targetfile, "MOV R%d, BP\n",r);
             ip += 2;
             fprintf(targetfile, "ADD R%d, %d\n",r,t->ptr1->lentry->binding+1);
             ip += 2;
           }
           else if(t->ptr1->lentry->isarg == 1) { //argument
             fprintf(targetfile, "MOV R%d, BP\n",r);
             ip += 2;
             fprintf(targetfile, "SUB R%d, %d\n",r,2+numarg);
             ip += 2;
             fprintf(targetfile, "ADD R%d, %d\n",r,t->ptr1->lentry->binding);
             ip += 2;
           }

         }

         else { //global variable
            loc = t->ptr1->gentry->binding;
            fprintf(targetfile, "MOV R%d, %d\n",r,loc);
            ip += 2;
          }

         if(t->ptr1->nodetype == 0){     // if its a non field
           if(t->ptr1->ptr1 != NULL){    // if its an array element
             r0 = codeGen(t->ptr1->ptr1 , targetfile);
             fprintf(targetfile, "ADD R%d, R%d\n",r,r0);
             ip += 2;
             free_reg();
           }               // by now r contains the location on which assignment stmt should work for a non field variable

         }

         else{
                 //if its a field...
            fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r contains heap pointer
            ip += 2;

            fprintf(targetfile, "ADD R%d, 1024\n",r); // adding 1024 to pointer gives memory address
            ip += 2;

            t = t->ptr1; // going to the fieldnode

            while(t->ptr1 != NULL){
              type = t->type;
              t = t->ptr1;
              fentry = FLookup(type,t->name);
              fprintf(targetfile, "ADD R%d, %d\n",r,fentry->fieldIndex); // location of the field in heap
              ip += 2;

              if(t->ptr1 != NULL){
                fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r has heap pointer of field
                ip += 2;
                fprintf(targetfile, "ADD R%d, 1024\n",r);
                ip += 2;
              }
            }

         }


          r0 = get_reg();
          r1 = get_reg();
          fprintf(targetfile, "BRKP\n");
          ip += 2;

          fprintf(targetfile, "MOV R%d, \"Read\"\n",r1); //function name
          ip += 2;

          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;

          fprintf(targetfile, "MOV R%d, -1\n",r1); //argument 1
          ip += 2;

          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;

          fprintf(targetfile, "MOV R%d, R%d\n",r1,r); //argument 2
          ip += 2;

          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;

          fprintf(targetfile, "PUSH R%d\n",r1); //argument 3
          ip += 2;

          fprintf(targetfile, "PUSH R%d\n",r1); //space for return value
          ip += 2;

          fprintf(targetfile, "CALL 0\n"); //call to library interface
          ip += 2;

          fprintf(targetfile, "BRKP\n");
          ip += 2;

          fprintf(targetfile, "POP R%d\n",r0); //get the return value
          ip += 2;

          fprintf(targetfile, "POP R%d\n",r1); //argument 3
          ip += 2;

          fprintf(targetfile, "POP R%d\n",r1); //argument 2
          ip += 2;

          fprintf(targetfile, "POP R%d\n",r1); //argument 1
          ip += 2;

          fprintf(targetfile, "POP R%d\n",r1); //function name
          ip += 2;

          free_reg();
          free_reg();
          free_reg();
          fprintf(targetfile, "BRKP\n");
          ip += 2;

          return -1;

          break;

          case 16 :     //WRITE

                    r0 = codeGen(t->ptr1 , targetfile);

                    fprintf(targetfile, "MOV [4096], R%d\n", r0);
                    ip += 2;

                    r1 = get_reg();
                    fprintf(targetfile, "MOV R%d, \"Write\"\n",r1); //function name
                    ip += 2;

                    fprintf(targetfile, "PUSH R%d\n",r1);
                    ip += 2;

                    fprintf(targetfile, "MOV R%d, -2\n",r1); //argument 1
                    ip += 2;

                    fprintf(targetfile, "PUSH R%d\n",r1);
                    ip += 2;

                    fprintf(targetfile, "MOV R%d, 4096\n",r1); //argument 2
                    ip += 2;

                    fprintf(targetfile, "PUSH R%d\n",r1);
                    ip += 2;

                    fprintf(targetfile, "PUSH R%d\n",r1); //argument 3
                    ip += 2;

                    fprintf(targetfile, "PUSH R%d\n",r1); //space for return value
                    ip += 2;

                    fprintf(targetfile, "CALL 0\n"); //call to library interface
                    ip += 2;

                    fprintf(targetfile, "BRKP\n");
                    ip += 2;

                    fprintf(targetfile, "POP R%d\n",r0); //get the return value
                    ip += 2;

                    fprintf(targetfile, "POP R%d\n",r1); //argument 3
                    ip += 2;

                    fprintf(targetfile, "POP R%d\n",r1); //argument 2
                    ip += 2;

                    fprintf(targetfile, "POP R%d\n",r1); //argument 1
                    ip += 2;

                    fprintf(targetfile, "POP R%d\n",r1); //function name
                    ip += 2;

                    free_reg();
                    free_reg();

                    fprintf(targetfile, "BRKP\n");
                    ip += 2;
                    return -1;

        case 17  :    //assignment stmt

             /*if(t->ptr1->lentry != NULL) { //local variable or argument
      					if(t->ptr1->lentry->isarg == 0) {//local variable
      						r0 = get_reg();
      						fprintf(targetfile, "MOV R%d, BP\n",r0);
      						ip += 2;
      						fprintf(targetfile, "ADD R%d, %d\n",r0,t->ptr1->lentry->binding+1);
      						ip += 2;

      						r1 = codeGen(t->ptr2, targetfile);
      						fprintf(targetfile, "MOV [R%d], R%d\n",r0,r1);
      						ip += 2;
      						free_reg();
      						free_reg();
      						return -1;
      					}

      					else if(t->ptr1->lentry->isarg == 1) { //argument
      						r0 = get_reg();
      						fprintf(targetfile, "MOV R%d, BP\n",r0);
      						ip += 2;
      						fprintf(targetfile, "SUB R%d, %d\n",r0,2+numarg);
      						ip += 2;
      						fprintf(targetfile, "ADD R%d, %d\n",r0,t->ptr1->lentry->binding);
      						ip += 2;

      						r1 = codeGen(t->ptr2, targetfile);
      						fprintf(targetfile, "MOV [R%d], R%d\n",r0,r1);
      						ip += 2;
      						free_reg();
      						free_reg();
      						return -1;
      					}


  				   }

             else if(t->ptr1->ptr1 == NULL){ //global variable
               r1 = codeGen(t->ptr2, targetfile);
               r = get_reg();
               loc = t->ptr1->gentry->binding;
               fprintf(targetfile, "MOV R%d, %d\n",r,loc);
               ip += 2;
             }

             else{
               r1 = codeGen(t->ptr2, targetfile);
               r = get_reg();
               r0 = codeGen(t->ptr1->ptr1 , targetfile);
               loc = t->ptr1->gentry->binding;
               fprintf(targetfile, "MOV R%d, %d\n",r,loc);
               ip += 2;
               fprintf(targetfile, "ADD R%d, R%d\n",r,r0);
               ip += 2;
               free_reg();
             }
             fprintf(targetfile, "MOV [R%d], R%d\n",r,r1);
             ip += 2;
             free_reg();
             free_reg();*/

             type = t->ptr1->type;
             r = get_reg();
             r1 = codeGen(t->ptr2,targetfile);  // code for expression

             if(t->ptr1->lentry != NULL) { //local variable or argument
              if(t->ptr1->lentry->isarg == 0) { //local variable
                fprintf(targetfile, "MOV R%d, BP\n",r);
                ip += 2;
                fprintf(targetfile, "ADD R%d, %d\n",r,t->ptr1->lentry->binding+1);
                ip += 2;
              }
              else if(t->ptr1->lentry->isarg == 1) { //argument
                fprintf(targetfile, "MOV R%d, BP\n",r);
                ip += 2;
                fprintf(targetfile, "SUB R%d, %d\n",r,2+numarg);
                ip += 2;
                fprintf(targetfile, "ADD R%d, %d\n",r,t->ptr1->lentry->binding);
                ip += 2;
              }

            }

            else { //global variable
               loc = t->ptr1->gentry->binding;
               fprintf(targetfile, "MOV R%d, %d\n",r,loc);
               ip += 2;
             }

            if(t->ptr1->nodetype == 0){     // if its a non field
              if(t->ptr1->ptr1 != NULL){    // if its an array element
                r0 = codeGen(t->ptr1->ptr1 , targetfile);
                fprintf(targetfile, "ADD R%d, R%d\n",r,r0);
                ip += 2;
                free_reg();
              }               // by now r contains the location on which assignment stmt should work for a non field variable

              fprintf(targetfile, "MOV [R%d], R%d\n",r,r1);
              ip += 2;
              free_reg(); // free r
              free_reg(); // free r1
              fprintf(targetfile, "BRKP\n");
              ip += 2;
              return -1;
            }

                //if its a field...
           fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r contains heap pointer
           ip += 2;

           fprintf(targetfile, "ADD R%d, 1024\n",r); // adding 1024 to pointer gives memory address
           ip += 2;

           t = t->ptr1; // going to the fieldnode

           while(t->ptr1 != NULL){
             type = t->type;
             t = t->ptr1;
             fentry = FLookup(type,t->name);
             fprintf(targetfile, "ADD R%d, %d\n",r,fentry->fieldIndex); // location of the field in heap
             ip += 2;

             if(t->ptr1 != NULL){
               fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r has heap pointer of field
               ip += 2;
               fprintf(targetfile, "ADD R%d, 1024\n",r);
               ip += 2;
             }
           }

           fprintf(targetfile, "MOV [R%d], R%d\n",r,r1); // now assignment has been done to proper location
           ip += 2;
           free_reg();
           free_reg();

           fprintf(targetfile, "BRKP\n");
           ip += 2;

           return -1;

        case 18 :     //conditional expression node

          r0 = codeGen(t->ptr1,targetfile);
    			label0 = labelGen();
          fprintf(targetfile, "BRKP\n");
          ip += 2;
    			fprintf(targetfile,"JZ R%d, L%d\n",r0,label0); //if false, jump to else or next
          ip += 2;
    			codeGen(t->ptr2,targetfile);

          if(t->ptr3 != NULL){
            label1 = labelGen();
            fprintf(targetfile, "JMP L%d\n",label1); //jump to end of if for true
            ip += 2;
      			fprintf(targetfile, "L%d:%d\n",label0,ip);
      			codeGen(t->ptr3, targetfile);    // else part
      			fprintf(targetfile, "L%d:%d\n",label1,ip);
          }

          else{
            fprintf(targetfile, "L%d:%d\n",label0,ip);
          }

          free_reg();
          return -1;

        case 19 :   //while

          /*while(evaluate(t->ptr1)){
            evaluate(t->ptr2);
          }*/

    			label0 = labelGen();
          fprintf(targetfile, "L%d:%d\n",label0,ip);

          r0 = codeGen(t->ptr1,targetfile);

          label1 = labelGen();
          top++;
          start[top] = label0;
          end[top] = label1;

          fprintf(targetfile, "BRKP\n");
          ip += 2;

    			fprintf(targetfile,"JZ R%d, L%d\n",r0,label1); //if false, jump to end of while
          ip += 2;

    			codeGen(t->ptr2,targetfile);

          fprintf(targetfile, "JMP L%d\n",label0); //jump back to condition
          ip += 2;
          fprintf(targetfile, "L%d:%d\n",label1,ip);
          top--;

          free_reg();
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          return -1;
          break;

        case 20 : //break
          fprintf(targetfile, "JMP L%d\n",end[top]);
          ip += 2;
          return -1;
          break;

        case 21 : //continue
          fprintf(targetfile, "JMP L%d\n",start[top]);
          ip += 2;
          return -1;
          break;

        case 22 : //function body
      		regno = -1;

      		//generate label:
      		fprintf(targetfile, "F%d:%d\n",GLookup(t->name)->binding,ip);
      		//generate code for pushing stuff
      		struct Gsymbol *gptr = GLookup(t->name);

      		if(strcmp(t->name, "main") != 0) {
      			//save the BP value of the caller to the stack and set BP to the top of the stack
      			memory++;
      			fprintf(targetfile, "ADD SP, 1\n");
      			ip += 2;
      			fprintf(targetfile, "MOV [SP], BP\n");
      			ip += 2;
      			fprintf(targetfile, "MOV BP, SP\n");
      			ip += 2;
      		}

      		//allocate space for local variables in the order that they appear in the declaration
      		struct Lsymbol *ltemp = t->lentry;
      		lsize = 0;
      		while(ltemp != NULL) {
      			if(ltemp->isarg == 0) { //local variable, not an argument
      				memory++; //all local variables have size 1
      				lsize++;
      			}
      			ltemp = ltemp->next;
      		}

      		fprintf(targetfile, "ADD SP, %d\n",lsize);
      		ip += 2;
          fprintf(targetfile, "BRKP\n");
          ip += 2;

      		codeGen(t->ptr1,targetfile);

      		if(strcmp(t->name, "main") != 0) {
      			//compute the return value and save it in the space reserved

            r0 = codeGen(t->ptr2, targetfile);
      			r1 = get_reg();
      			fprintf(targetfile, "MOV R%d, BP\n", r1);
      			ip += 2;
      			fprintf(targetfile, "SUB R%d, 2\n",r1);
      			ip += 2;
      			fprintf(targetfile, "MOV [R%d], R%d\n",r1,r0);
      			ip += 2;
      			free_reg();
      			free_reg();

      			//pop out the local variables
      			struct Lsymbol *ltemp = t->lentry;
      			while(ltemp != NULL) {
      				memory--;
      				ltemp = ltemp->next;
      			}
      			fprintf(targetfile, "SUB SP, %d\n",lsize);
      			ip += 2;

      			//pop and save the old BP value
      			fprintf(targetfile, "MOV BP, [SP]\n");
            ip += 2;
      			fprintf(targetfile, "SUB SP, 1\n");
      			ip += 2;
      			memory--;
            fprintf(targetfile, "BRKP\n");
            ip += 2;

      			//return
      			fprintf(targetfile, "RET\n");
      			ip += 2;
      			memory--;
          }

    			return -1;

          break;

        case 23 : //function invocation
          //save registers in use
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          rcount = 0;
          while(rcount <= regno) { // regno is the register number of last register in use
            memory++;
            fprintf(targetfile, "ADD SP, 1\n");
            ip += 2;
            fprintf(targetfile, "MOV [SP], R%d\n",rcount);
            ip += 2;
            rcount++;
          }

          //fprintf(targetfile, "MOV SP, %d\n",memory);
          //regno = 0;

          //push arguments
          struct exprlist *temp = t->arglist;
          while(temp != NULL) {
            r0 = codeGen(temp->t, targetfile);
            memory++;
            fprintf(targetfile, "ADD SP, 1\n");
            ip += 2;
            fprintf(targetfile, "MOV [SP], R%d\n",r0);
            ip += 2;
            free_reg();
            temp = temp->next;
          }

          //push one empty space on the stack for the return value
          memory++;
          fprintf(targetfile, "ADD SP, 1\n");
          ip += 2;
          retspace = memory;
          //fprintf(targetfile, "MOV SP, %d\n",memory);
          //ip += 2;

          //memory++; //push the return address on the stack
          //fprintf(targetfile, "ADD SP, 1\n");
          //ip += 2;

          //call the function
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          fprintf(targetfile, "CALL F%d\n",GLookup(t->name)->binding);
          ip += 2;

          //retrieve the return value from the stack and save it to a register;
          r0 = rcount;
          //memory = retspace;

          fprintf(targetfile, "MOV R%d, [SP]\n",r0);
          ip += 2;
          memory--;
          fprintf(targetfile, "SUB SP, 1\n");
          ip += 2;

          //pop off the arguments
          temp = t->arglist;
          while(temp != NULL) {
            memory--;
            fprintf(targetfile, "SUB SP, 1\n");
            ip += 2;
            temp = temp->next;
          }

          //restore saved register context
          rcount = rcount - 1;
          while(rcount >= 0) {
            fprintf(targetfile, "MOV R%d, [SP]\n",rcount);
            ip += 2;
            memory--;
            fprintf(targetfile, "SUB SP, 1\n");
            ip += 2;
            rcount--;
          }

          regno = r0;
          fprintf(targetfile, "BRKP\n");
          ip += 2;

          return r0;
          break;


        case 24 : // Field
            type = t->type;
            r = get_reg();

            fprintf(targetfile, "BRKP\n");
            ip += 2;

            if(t->lentry != NULL) { //local variable or argument
             if(t->lentry->isarg == 0) { //local variable
               fprintf(targetfile, "MOV R%d, BP\n",r);
               ip += 2;
               fprintf(targetfile, "ADD R%d, %d\n",r,t->lentry->binding+1);
               ip += 2;
               fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r contains heap pointer
               ip += 2;

             }
             else if(t->lentry->isarg == 1) { //argument
               fprintf(targetfile, "MOV R%d, BP\n",r);
               ip += 2;
               fprintf(targetfile, "SUB R%d, %d\n",r,2+numarg);
               ip += 2;
               fprintf(targetfile, "ADD R%d, %d\n",r,t->lentry->binding);
               ip += 2;
               fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r contains heap pointer
               ip += 2;
             }
           }

           else { //global variable
              loc = t->gentry->binding;
              fprintf(targetfile, "MOV R%d, [%d]\n",r,loc);
              ip += 2;
            }

          fprintf(targetfile, "ADD R%d, 1024\n",r); // adding 1024 to pointer gives memory address
          ip += 2;

          while(t->ptr1 != NULL){
            type = t->type;
            t = t->ptr1;
            fentry = FLookup(type,t->name);
            fprintf(targetfile, "ADD R%d, %d\n",r,fentry->fieldIndex); // location of the field in heap
            ip += 2;

            if(t->ptr1 != NULL){
              fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r has heap pointer of field
              ip += 2;
              fprintf(targetfile, "ADD R%d, 1024\n",r);
              ip += 2;
            }
          }

          fprintf(targetfile, "BRKP\n");
          ip += 2;

          fprintf(targetfile, "MOV R%d, [R%d]\n",r,r); // now r has required value
          ip += 2;

          fprintf(targetfile, "BRKP\n");
          ip += 2;
          return r;

          break;


        case 25 : // Free()
          r0 = get_reg();
          r1 = get_reg();
          r = codeGen(t->ptr1,targetfile);
          fprintf(targetfile, "MOV R%d, \"Free\"\n",r1); //function name
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;
          fprintf(targetfile, "MOV R%d, R%d\n",r1,r); //argument 1 - for Free argument 1 is pointer (heap location) which is to be freed
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1); //argument 2
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1); //argument 3
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1); //space for return value
          ip += 2;
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          fprintf(targetfile, "CALL 0\n"); //call to library interface
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r0); //get the return value to r0
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //argument 3
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //argument 2
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //argument 1
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //function name
          ip += 2;
          free_reg();
          free_reg();
          free_reg();
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          return -1;
          break;

        case 26 : // Alloc()
          r0 = get_reg();
          r1 = get_reg();
          fprintf(targetfile, "MOV R%d, \"Alloc\"\n",r1); //function name
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;
          fprintf(targetfile, "MOV R%d, 8\n",r1); //argument 1 - for Alloc argument 1 is size which is = 8 words
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1);
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1); //argument 2
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1); //argument 3
          ip += 2;
          fprintf(targetfile, "PUSH R%d\n",r1); //space for return value
          ip += 2;
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          fprintf(targetfile, "CALL 0\n"); //call to library interface
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r0); //get the return value to r0
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //argument 3
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //argument 2
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //argument 1
          ip += 2;
          fprintf(targetfile, "POP R%d\n",r1); //function name
          ip += 2;
          free_reg();
          fprintf(targetfile, "BRKP\n");
          ip += 2;
          return r0;
          break;

        case 27 : // null
          r = get_reg();
          fprintf(targetfile, "MOV R%d, -1\n",r);
          ip += 2;
          return r;
          break;

        default : return 0;

    }

    return 0;
}


//FUNCTION TO ADD XSM CODE FOR EXIT
void exitGen(FILE *targetfile) {
				   int r0 = get_reg();
				   /*int r1 = get_reg();
				   fprintf(targetfile, "MOV R%d, \"Exit\"\n",r1); //function name
           ip += 2;

				   fprintf(targetfile, "PUSH R%d\n",r1);
           ip += 2;

				   fprintf(targetfile, "PUSH R%d\n",r1);    //argument 1
           ip += 2;

				   fprintf(targetfile, "PUSH R%d\n",r1);    //argument 2
           ip += 2;

				   fprintf(targetfile, "PUSH R%d\n",r1);    //argument 3
           ip += 2;

				   fprintf(targetfile, "PUSH R%d\n",r1);    //space for return value
           ip += 2;

				   fprintf(targetfile, "CALL 0\n");      //call to library
           ip += 2;

				   fprintf(targetfile, "POP R%d\n",r0);     //get the return value
           ip += 2;

				   fprintf(targetfile, "POP R%d\n",r1);     //argument 3
           ip += 2;

				   fprintf(targetfile, "POP R%d\n",r1);     //argument 2
           ip += 2;

				   fprintf(targetfile, "POP R%d\n",r1);     //argument 1
           ip += 2;

				   fprintf(targetfile, "POP R%d\n",r1);     //function name
           ip += 2;

           fprintf(targetfile, "BRKP");
           ip += 2;

				   free_reg();*/

           fprintf(targetfile, "MOV R%d, 10\n",r0); //function name
           ip += 2;

           fprintf(targetfile, "PUSH R%d\n",r0);  //argument 1
           ip += 2;

           fprintf(targetfile, "PUSH R%d\n",r0);  //argument 2
           ip += 2;

           fprintf(targetfile, "PUSH R%d\n",r0);   //argument 3
           ip += 2;

           fprintf(targetfile, "PUSH R%d\n",r0);   //return value
           ip += 2;

           fprintf(targetfile, "INT 10\n");       //interrupt call
           ip += 2;
				   free_reg();
}
