#include<stdio.h>
#include<string.h>

struct Argument *ArgLookup(struct Argument *args, char *name) { //pointer to the head of the arlist passed as argument
	struct Argument *temp = args;
	while(temp!=NULL) {
		if(!strcmp(temp->name,name)) {
			return temp;
		}
		temp = temp->next;
	}
	return NULL;
}

struct Gsymbol* GInstall(char *name, struct Typetable *type, int size , struct Argument *args){

  struct Gsymbol *temp;

  if(GLookup(name) != NULL){
    printf("Redeclaration of %s\n",name);
    exit(1);
  }

  temp = (struct Gsymbol*)malloc(sizeof(struct Gsymbol));
  temp->name = strdup(name);
  temp->type = type;
  temp->size = size;
	temp->args = args;

	if(temp->size == 0){ // function
		temp->binding = fbind++;
	}
	else{
		temp->binding = 4097 + memory;
	}

  temp->next = HEAD;

  memory = memory + size;

  HEAD = temp;

  return temp;
}



struct Gsymbol* GLookup(char *name){

  struct Gsymbol *temp;

  temp = HEAD;

  if(name == NULL){
    return NULL;
  }

  while(temp != NULL)
  {
    if(!strcmp(temp->name , name))
      break;

    temp = temp->next;
  }

  return temp;
}

struct Lsymbol *LInstall(char *name, struct Typetable *type , int isarg) {
	struct Lsymbol *temp = malloc(sizeof(struct Lsymbol));
	temp->name = strdup(name);
	temp->type = type;
	temp->isarg = isarg;
	/*if(lhead == NULL) {
		lhead = temp;
		temp->next = NULL;
	}
	else {
		temp->next = lhead;
		lhead = temp;
	}*/

	if(temp->isarg == 1){
		temp->binding = numarg++;
	}

	else{
		temp->binding = numloc++;
	}
	temp->next = lhead;
	lhead = temp;
	return lhead;
}


struct Lsymbol *LLookup(char *name) {
	struct Lsymbol *temp = lhead;
	while(temp != NULL) {
		if(!strcmp(temp->name, name)) {
			return temp;
		}
		else {
			temp = temp->next;
		}
	}
	return NULL;
}



void typeTableCreate(){
	struct Typetable *temp;

	temp = malloc(sizeof(struct Typetable));

	temp->name = strdup("integer");
	temp->size = 1;
	temp->fields = NULL;
	temp->next = NULL;

	thead = temp;

	temp = malloc(sizeof(struct Typetable));

	temp->name = strdup("boolean");
	temp->size = 1;
	temp->fields = NULL;
	temp->next = thead;
	thead = temp;

	temp = malloc(sizeof(struct Typetable));

	temp->name = strdup("void");
	temp->size = 0;
	temp->fields = NULL;
	temp->next = thead;
	thead = temp;

	temp = malloc(sizeof(struct Typetable));

	temp->name = strdup("null");
	temp->size = 0;
	temp->fields = NULL;
	temp->next = thead;
	thead = temp;
}

struct Typetable* TLookup(char *name){
	struct Typetable *temp;

	temp = thead;

	while(temp != NULL){
		if(!strcmp(temp->name , name)){
			return temp;
		}

		temp = temp->next;
	}

	return NULL;
}

struct Typetable * TInstall(char *name , int size , struct Fieldlist *fields){
	struct Typetable *temp;

	temp = malloc(sizeof(struct Typetable));
	temp->name = strdup(name);
	temp->size = size;
	temp->fields = fields;
	temp->next = thead;
	thead = temp;

	return temp;
}

struct Fieldlist* FLookup(struct Typetable *type, char *name){
	struct Fieldlist *temp;

	temp = type->fields;

	while (temp != NULL) {
		if(!strcmp(temp->name,name)){
			return temp;
		}

		temp = temp->next;
	}

	return NULL;
}

int GetSize(struct Typetable *type){
	int s = 0;

	if(type->size > 0){
		return type->size;
	}

	struct Fieldlist *temp;

	temp = type->fields;

	while(temp != NULL){
		s += GetSize(temp->type);
		temp =temp->next;
	}

	return s;
}
