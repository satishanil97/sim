/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_YY_Y_TAB_H_INCLUDED
# define YY_YY_Y_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    NUM = 258,
    ID = 259,
    INTEGER = 260,
    BOOLEAN = 261,
    DECL = 262,
    ENDDECL = 263,
    BEG = 264,
    END = 265,
    TYPE = 266,
    ENDTYPE = 267,
    null = 268,
    WRITE = 269,
    ASGN = 270,
    READ = 271,
    ALLOC = 272,
    FREE = 273,
    IF = 274,
    THEN = 275,
    ELSE = 276,
    ENDIF = 277,
    WHILE = 278,
    DO = 279,
    ENDWHILE = 280,
    BREAK = 281,
    CONTINUE = 282,
    MAIN = 283,
    RET = 284,
    LT = 285,
    GT = 286,
    LE = 287,
    GE = 288,
    EQ = 289,
    NE = 290,
    AND = 291,
    OR = 292,
    PLUS = 293,
    MINUS = 294,
    MUL = 295,
    DIV = 296
  };
#endif
/* Tokens.  */
#define NUM 258
#define ID 259
#define INTEGER 260
#define BOOLEAN 261
#define DECL 262
#define ENDDECL 263
#define BEG 264
#define END 265
#define TYPE 266
#define ENDTYPE 267
#define null 268
#define WRITE 269
#define ASGN 270
#define READ 271
#define ALLOC 272
#define FREE 273
#define IF 274
#define THEN 275
#define ELSE 276
#define ENDIF 277
#define WHILE 278
#define DO 279
#define ENDWHILE 280
#define BREAK 281
#define CONTINUE 282
#define MAIN 283
#define RET 284
#define LT 285
#define GT 286
#define LE 287
#define GE 288
#define EQ 289
#define NE 290
#define AND 291
#define OR 292
#define PLUS 293
#define MINUS 294
#define MUL 295
#define DIV 296

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 30 "ast.y" /* yacc.c:1909  */

	struct tnode* node;
	int integer;
	char *string;

#line 142 "y.tab.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_Y_TAB_H_INCLUDED  */
