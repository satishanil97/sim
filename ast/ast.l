%{
	#include <stdio.h>
	#include "y.tab.h"
	#include <stdlib.h>

	int number;

	//[-][0-9]+	{char *neg; neg = yytext + 1; number = -1*atoi(neg); yylval.node = createNode(1,1,number,NULL,NULL,NULL,NULL,NULL); return NUM;} FOR -ve NUMBERS

%}

%%
[0-9]+	{number = atoi(yytext); yylval.node = createNode(TLookup("integer"),1,number,NULL,NULL,NULL,NULL,NULL); return NUM;}
"+"	{return PLUS;}
"." {return *yytext;}
"-"	{return MINUS;}
"*"	{return MUL;}
"/"	{return DIV;}
"type" {return TYPE;}
"endtype" {return ENDTYPE;}
"NULL" {return null;}
"decl" {return DECL;}
"main" {return MAIN;}
"return" {return RET;}
"integer" {return INTEGER;}
"boolean" {return BOOLEAN;}
"enddecl" {return ENDDECL;}
"begin" {return BEG;}
"end" {return END;}
"alloc" {return ALLOC;}
"free" {return FREE;}
"if"	{return IF;}
"then"	{return THEN;}
"else"	{return ELSE;}
"endif"	{return ENDIF;}
"write" {return WRITE;}
"while" {return WHILE;}
"read" {return READ;}
"endwhile" {return ENDWHILE;}
"do" {return DO;}
"break" {return BREAK;}
"continue" {return CONTINUE;}
"<="		{return LE;}
"<"			{return LT;}
"=="		{return EQ;}
"!="		{return NE;}
">="		{return GE;}
">"			{return GT;}
"="			{return ASGN;}
"&&"		{return AND;}
"||"		{return OR;}
"["			{return *yytext;}
"]"			{return *yytext;}
"{" 		{return *yytext;}
"}"			{return *yytext;}
[a-zA-Z][a-zA-Z0-9]*		{yylval.string = strdup(yytext); return ID;}
[;]	{return *yytext;}
[,] {return *yytext;}
[\t]	{}
[" "] {}
[()]	{return *yytext;}
[\n]	{}
.	{printf("\n%s ",yytext);yyerror("unknown character\n");exit(1);}

%%

int yywrap(void) {
	return 1;
}
