%{
	#include <stdio.h>
	#include <stdlib.h>
	#include<string.h>
	#include "gsymbol.h"
	#include "gsymbol.c"
	#include "ast.h"
	#include "ast.c"

	int yylex(void);

	struct varlist{
		char *NAME;
		int size;
		struct Argument *args;
		struct varlist * next;
	}*vhead;

	struct Argument *args;
	struct Fieldlist *fieldlist;

	int idtype,idsize;

	FILE *targetfile;
	char *filename;

	extern FILE* yyin;
%}

%union{
	struct tnode* node;
	int integer;
	char *string;
}

%type <node> expr
%type <node> stmt
%type <node> body
%type <node> slist
%type <node> Program fdefs mainblock fdef fdefpart1 fdefpart2 mainblockpart1 mainblockpart2
%type <node> dataField
%type <string> typename
%token <node> NUM
%token <string> ID
%token INTEGER BOOLEAN DECL ENDDECL BEG END TYPE ENDTYPE null
%token WRITE ASGN READ ALLOC FREE
%token IF THEN ELSE ENDIF WHILE DO ENDWHILE BREAK CONTINUE MAIN RET
%nonassoc LT GT LE GE EQ NE
%left AND OR
%left PLUS MINUS
%left MUL DIV

%%

Program : typedefs declarations fdefs mainblock {
							codeGen($4,targetfile);
							exitGen(targetfile);
							exit(1);
						}
				| typedefs declarations mainblock {
							     codeGen($3, targetfile);
							     exitGen(targetfile);
							     exit(1);

							}

				| declarations fdefs mainblock {
											codeGen($3,targetfile);
											exitGen(targetfile);
											exit(1);
										}

				| declarations mainblock {
							     codeGen($2, targetfile);
							     exitGen(targetfile);
							     exit(1);

							}

				| typedefs mainblock	{
									headerGen(targetfile);
									codeGen($2, targetfile);
									exitGen(targetfile);
									exit(1);
								}


				| mainblock {
									 headerGen(targetfile);
							     codeGen($1, targetfile);
							     exitGen(targetfile);
							     exit(1);

							}
        ;

typedefs : TYPE typeDefList ENDTYPE
				 ;

typeDefList : typeDefList typeDef
						| typeDef
						;

typeDef : typeDef1 typeDef2 {
															int size = 0;
															struct Fieldlist *temp;
															struct Typetable *ptr;

															temp = fieldlist;

															while(temp != NULL){
																if(temp->type != NULL){
																	size += temp->type->size;
																}
																else{
																	size++;		//recursive definitions are like pointers -- takes 1 word
																}

																temp = temp->next;
															}


															ptr = TInstall(curr_type , size , fieldlist);

															temp = fieldlist;

															while(temp != NULL){
																if(temp->type == NULL){ 		//recursive definition
																	temp->type = ptr;
																}

																temp = temp->next;
															}

															fieldlist = NULL;
															findex = 0;
												}

typeDef1 : ID '{' { curr_type = strdup($1); }
				;

typeDef2 : fieldList '}'
				 ;

fieldList : fieldList field
					| field
					;

field : typename ID ';' {
	struct Fieldlist *temp;
	temp = malloc(sizeof(struct Fieldlist));
	temp->name = strdup($2);
	if(findex == 8){
		printf("Type %s has more than 8 fields\n",curr_type);
		exit(1);
	}
	temp->fieldIndex = findex++;
	temp->type = TLookup($1);
	temp->next = NULL;

	struct Fieldlist *ptr;
	ptr = fieldlist;
	if(ptr != NULL){
		while(ptr->next != NULL){
			ptr = ptr->next;
		}

		ptr->next = temp;
	}

	else{
		fieldlist = temp;
	}

}

typename : INTEGER {$$ = strdup("integer");}
				 | BOOLEAN {$$ = strdup("boolean");}
				 | ID {
					 if((TLookup($1) == NULL) && strcmp(curr_type,$1)){
						 printf("Error : type %s should be declared before type %s\n",$1,curr_type);
						 exit(1);
					 }

					 $$ = strdup($1);
				 }
				 ;

declarations : DECL declist ENDDECL {

									printf("Declarations Complete\n");
					    		struct Gsymbol *temp = HEAD;

									while(temp != NULL) {
										printf("%s, ", temp->name);
										temp = temp->next;
									}
									printf("\n");

									headerGen(targetfile);
								}
					     ;

declist : decl
				| declist decl
				;

decl : typename idlist ';' {
															struct varlist *temp;
															temp = vhead;
															while(temp != NULL){
																GInstall(temp->NAME , TLookup($1) , temp->size , temp->args);
																temp = temp->next;
															}
												}
		 ;

idlist : idlist ',' ID {
							 struct varlist * temp;
							 temp = malloc(sizeof(struct varlist));
							 temp->NAME = strdup($3);
							 temp->size = 1;
							 temp->next = vhead;
							 vhead = temp;

						}

			 | idlist ',' ID '[' NUM ']' {
				 struct varlist * temp;
				 temp = malloc(sizeof(struct varlist));
				 temp->NAME = strdup($3);
				 temp->size = $5->val;
				 temp->next = vhead;
				 vhead = temp;
			 }

			 | ID '[' NUM ']' {
				 struct varlist * temp;
				 temp = malloc(sizeof(struct varlist));
				 temp->NAME = strdup($1);
				 temp->size = $3->val;
				 temp->next = NULL;
				 vhead = temp;
			 }

			 | ID {
				 struct varlist * temp;
				 temp = malloc(sizeof(struct varlist));
				 temp->NAME = strdup($1);
				 temp->size = 1;
				 temp->next = NULL;
				 vhead = temp;
			 }

			 |ID '(' arglist ')' {struct varlist *temp;
		 		  	     temp = malloc(sizeof(struct varlist));
		 			     temp->NAME = strdup($1);
		 			     temp->size = 0;
		 			     temp->args = args; //include args struct
		 			     temp->next = NULL;
		 			     vhead = temp;
		 			   }
		 	|idlist ',' ID '(' arglist ')' { struct varlist *temp;
		 			  	     temp = malloc(sizeof(struct varlist));
		 				     temp->NAME = strdup($3);
		 				     temp->size = 0;
		 				     temp->args = args; //include args struct
		 				     temp->next = vhead;
		 				     vhead = temp;
		 				}
			 ;

arglist :	{
			        			args = NULL;
			 		}
			 	| arglist ',' typename ID {
			 				struct Argument *temp,*ptr;
			 				temp = malloc(sizeof(struct Argument));
			 				temp->name = strdup($4);
			 				temp->type = TLookup($3);

							ptr = args;
							if(ptr != NULL){
								while(ptr->next != NULL){
									ptr = ptr->next;
								}
							}

			 				ptr->next = temp;
			 			}

			 	| typename ID	{
			 				struct Argument *temp = malloc(sizeof(struct Argument));
			 				temp->name = strdup($2);
			 				temp->type = TLookup($1);

			 				temp->next = NULL;
			 				args = temp;
			 			}
			 	;


fdefs : fdefs fdef {	codeGen($2, targetfile);
											numarg = 0;
											numloc = 0;
											lhead = NULL;
									}
			| fdef {	codeGen($1, targetfile);
								numarg = 0;
								numloc = 0;
								lhead = NULL;
					}
			;

fdef : fdefpart1 fdefpart2 {$$ = $2; printf("Function  %s declaration done\n",curr_func);}
		 ;

fdefpart1 : typename ID '(' arglist ')' '{' {
													curr_func = strdup($2);
													if(GLookup(curr_func)==NULL) {
														printf("Function %s Undeclared\n",curr_func);
														exit(1);
													}
													if(strcmp(GLookup(curr_func)->type->name , $1)) {
														printf("Type Mismatch in return type of function %s\n",curr_func);
														exit(1);
													}

													struct Argument *ptr = GLookup(curr_func)->args;
													struct Argument *temp = args;

													printf("Function %s -> Arguments : \t",curr_func);

													while(ptr != NULL) {
														if(temp == NULL || strcmp(ptr->type->name , temp->type->name) || strcmp(ptr->name, temp->name)!=0) {
															printf("Mismatch in arguments in function %s\n",curr_func);
															if(temp == NULL){
																printf("Not enough arguments passed to function %s\n",curr_func);
															}
															exit(1);
														}

														else{
															printf("%s , ",temp->name);
														}

														LInstall(temp->name,temp->type,1);
														ptr = ptr->next;
														temp = temp->next;
													}

													printf("\n");

													if(temp != NULL){
														printf("Too many arguments for function %s\n",curr_func);
													}
											}

					;

fdefpart2 : ldeclarations body '}'	{$$ = $2;}
					|	body '}' {$$ = $1;}
					;

mainblock : mainblockpart1 mainblockpart2 { $$ = $2; }
					;

mainblockpart1 : INTEGER MAIN '(' arglist  ')' '{' {
		       					curr_func = strdup("main");
										if(args != NULL) {
											printf("Mismatch in arguments for main\n");
											exit(1);
										}
								}

	       ;

mainblockpart2 : ldeclarations body '}' { $$ = $2; printf("Function  %s declaration done\n",curr_func);}
							 | body	'}' {$$ = $1; printf("Function  %s declaration done\n",curr_func);}

ldeclarations : DECL ldecllist ENDDECL {
														printf("Local Declarations Complete : \t");
														struct Lsymbol *temp = lhead;

														while(temp != NULL) {
															printf("%s, ", temp->name);
															temp = temp->next;
														}
														printf("\n");
													}
							;

ldecllist : ldecllist ldecl
					| ldecl
					;

ldecl : typename lidlist ';' {
										struct varlist *temp;
										temp = vhead;
										while(temp != NULL) {
											if(ArgLookup(GLookup(curr_func)->args, temp->NAME) != NULL) {
												printf("Local Variable and Argument share name - %s\n",temp->NAME);
												exit(1);
											}
											LInstall(temp->NAME, TLookup($1) , 0);
											temp = temp->next;
										}

										vhead = NULL;
								}

				;

lidlist : lidlist ',' ID {
										struct varlist *temp,*ptr;
										temp = malloc(sizeof(struct varlist));
										temp->NAME = $3;
										temp->size = 1;
										temp->args = NULL;
										ptr = vhead;
										if(ptr != NULL){
											while(ptr->next != NULL){
												ptr = ptr->next;
											}
										}

						 				ptr->next = temp;
									}

				| ID {
									struct varlist *temp;
									temp = malloc(sizeof(struct varlist));
									temp->NAME = $1;
									temp->size = 1;
									temp->args = NULL;
									temp->next = NULL;
									vhead = temp;
							}

				;



body : BEG slist RET expr ';' END	{//$$ = $2
					struct Typetable *type = GLookup(curr_func)->type;
					/*if(type != $4->type){
						printf("Function and return type mismatch for %s()\n",curr_func);
						exit(1);
					}*/
					struct tnode *bodynode = createNode(type,22,0,curr_func,$2,$4,NULL,NULL);
					bodynode->lentry = lhead;
					$$ = bodynode;
				}
		 ;

slist : slist stmt 	{
						$$ = createNode(TLookup("void"),14,0,NULL,$1,$2,NULL,NULL);
					}
      | stmt 	{
				$$ = $1;
			}
      ;

stmt : ID ASGN expr ';'      {

																struct tnode *idnode;

																struct Lsymbol *lentry = LLookup($1);

																if(lentry != NULL){
																	idnode = createNode(lentry->type,0,0,$1,NULL,NULL,NULL,NULL);
																}

																else{
																	struct Gsymbol *entry = GLookup($1);
																	if(entry == NULL){
																		printf("Undeclared variable %s\n",$1);
																		exit(1);
																	}
																	idnode = createNode(entry->type,0,0,$1,NULL,NULL,NULL,NULL);
																}

																$$ = createNode(TLookup("void"),17,0,NULL,idnode,$3,NULL,NULL);
                             }

			| ID '[' expr ']' ASGN expr ';'	{
																				struct Gsymbol *entry = GLookup($1);
																				if(entry == NULL){
																					printf("Undeclared variable %s\n",$1);
																					exit(1);
																				}

																				struct tnode *idnode = createNode(entry->type,0,0,$1,$3,NULL,NULL,NULL);
																				$$ = createNode(TLookup("void"),17,0,NULL,idnode,$6,NULL,NULL);
																			}


			| dataField ASGN expr ';' { $$ = createNode(TLookup("void"),17,0,NULL,$1,$3,NULL,NULL); }


			| dataField ASGN ALLOC '(' ')' ';' {
																						struct tnode *allocnode;

																						allocnode = createNode(TLookup("integer"),26,0,NULL,NULL,NULL,NULL,NULL);

																						$$ = createNode(TLookup("void"),17,0,NULL,$1,allocnode,NULL,NULL);
																				}


			| ID ASGN ALLOC '(' ')' ';' {
																		struct tnode *idnode,*allocnode;

																		struct Lsymbol *lentry = LLookup($1);

																		if(lentry != NULL){
																			idnode = createNode(lentry->type,0,0,$1,NULL,NULL,NULL,NULL);
																		}

																		else{
																			struct Gsymbol *entry = GLookup($1);
																			if(entry == NULL){
																				printf("Undeclared variable %s\n",$1);
																				exit(1);
																			}
																			idnode = createNode(entry->type,0,0,$1,NULL,NULL,NULL,NULL);
																		}

																		allocnode = createNode(idnode->type,26,0,NULL,NULL,NULL,NULL,NULL);

																		$$ = createNode(TLookup("void"),17,0,NULL,idnode,allocnode,NULL,NULL);
																}


			| FREE '(' ID ')' ';' {													// here ID is a user defined variable
															struct tnode *idnode;

															struct Lsymbol *lentry = LLookup($3);

															if(lentry != NULL){
																idnode = createNode(lentry->type,0,0,$3,NULL,NULL,NULL,NULL);
															}

															else{
																struct Gsymbol *entry = GLookup($3);
																if(entry == NULL){
																	printf("Undeclared variable %s\n",$3);
																	exit(1);
																}
																idnode = createNode(entry->type,0,0,$3,NULL,NULL,NULL,NULL);
															}

															$$ = createNode(TLookup("void"),25,0,NULL,idnode,NULL,NULL,NULL);
													}


			| FREE '(' dataField ')' ';' { $$ = createNode(TLookup("void"),25,0,NULL,$3,NULL,NULL,NULL); }


			| READ '(' ID ')' ';'  	{
																struct tnode *idnode;

																struct Lsymbol *lentry = LLookup($3);

																if(lentry != NULL){
																	idnode = createNode(lentry->type,0,0,$3,NULL,NULL,NULL,NULL);
																}

																else{
																	struct Gsymbol *entry = GLookup($3);
																	if(entry == NULL){
																		printf("Undeclared variable %s\n",$3);
																		exit(1);
																	}
																	idnode = createNode(entry->type,0,0,$3,NULL,NULL,NULL,NULL);
																}

                                $$ = createNode(TLookup("void"),15,0,NULL,idnode,NULL,NULL,NULL);
                              }


			| READ '(' ID '[' expr ']' ')' ';'{
																					struct Gsymbol *entry = GLookup($3);
																					if(entry == NULL){
																						printf("Undeclared variable %s\n",$3);
																						exit(1);
																					}

																					struct tnode *idnode = createNode(entry->type,0,0,$3,$5,NULL,NULL,NULL);
																					$$ = createNode(TLookup("void"),15,0,NULL,idnode,NULL,NULL,NULL);
																				}


      | READ '(' dataField ')' ';' { $$ = createNode(TLookup("void"),15,0,NULL,$3,NULL,NULL,NULL); }


			| WRITE '(' expr ')' ';' 	{
																	$$ = createNode(TLookup("void"),16,0,NULL,$3,NULL,NULL,NULL);
                            	}



			|	IF expr THEN  slist ENDIF ';'	{
				$$ = createNode(TLookup("void"),18,0,NULL,$2,$4,NULL,NULL);
			}

			| IF expr THEN  slist ELSE  slist ENDIF ';'	{
				$$ = createNode(TLookup("void"),18,0,NULL,$2,$4,$6,NULL);
			}

			| WHILE expr DO  slist ENDWHILE ';' {
				$$ = createNode(TLookup("void"),19,0,NULL,$2,$4,NULL,NULL);
			}

			| BREAK ';' {
				$$ = createNode(TLookup("void"),20,0,NULL,NULL,NULL,NULL,NULL);
			}

			| CONTINUE ';' {
				$$ = createNode(TLookup("void"),21,0,NULL,NULL,NULL,NULL,NULL);
			}

			;


expr : expr PLUS expr		{$$ = createNode(TLookup("integer"),2,0,NULL,$1,$3,NULL,NULL);}
			| expr MINUS expr  	{$$ = createNode(TLookup("integer"),3,0,NULL,$1,$3,NULL,NULL);}
			| expr MUL expr	{$$ = createNode(TLookup("integer"),4,0,NULL,$1,$3,NULL,NULL);}
			| expr DIV expr	{$$ = createNode(TLookup("integer"),5,0,NULL,$1,$3,NULL,NULL);}
			| expr LT expr	{$$ = createNode(TLookup("boolean"),6,0,NULL,$1,$3,NULL,NULL);}
			| expr LE expr	{$$ = createNode(TLookup("boolean"),7,0,NULL,$1,$3,NULL,NULL);}
			| expr EQ expr	{$$ = createNode(TLookup("boolean"),8,0,NULL,$1,$3,NULL,NULL);}
			| expr GE expr	{$$ = createNode(TLookup("boolean"),9,0,NULL,$1,$3,NULL,NULL);}
			| expr GT expr 	{$$ = createNode(TLookup("boolean"),10,0,NULL,$1,$3,NULL,NULL);}
			| expr NE expr	{$$ = createNode(TLookup("boolean"),11,0,NULL,$1,$3,NULL,NULL);}
			| expr AND expr	{$$ = createNode(TLookup("boolean"),12,0,NULL,$1,$3,NULL,NULL);}
			| expr OR expr	{$$ = createNode(TLookup("boolean"),13,0,NULL,$1,$3,NULL,NULL);}
			| '(' expr ')'		{$$ = $2;}
			| NUM			{$$ = $1;}
			| null		{$$ = createNode(TLookup("null"),27,-1,NULL,NULL,NULL,NULL,NULL);}
			| ID '[' expr ']' {
				struct Gsymbol *entry = GLookup($1);
				if(entry == NULL){
					printf("Undeclared variable %s\n",$1);
					exit(1);
				}

				struct tnode *idnode = createNode(entry->type,0,0,$1,$3,NULL,NULL,NULL);
				$$ = idnode;
			}

			| ID '(' explist ')' {
				struct Gsymbol *entry = GLookup($1);
				if(entry == NULL){
					printf("Undeclared function %s\n",$1);
					exit(1);
				}
				$$ = createNode(entry->type,23,0,$1,NULL,NULL,NULL,ehead[top]);

				ehead[top--] = NULL;
			}

			| ID '(' ')' {
				struct Gsymbol *entry = GLookup($1);
				if(entry == NULL){
					printf("Undeclared function %s\n",$1);
					exit(1);
				}
				$$ = createNode(entry->type,23,0,$1,NULL,NULL,NULL,NULL);
			}

			| ID {

							struct tnode *idnode;

							struct Lsymbol *lentry = LLookup($1);

							if(lentry != NULL){
								idnode = createNode(lentry->type,0,0,$1,NULL,NULL,NULL,NULL);
							}

							else{
								struct Gsymbol *entry = GLookup($1);
								if(entry == NULL){
									printf("Undeclared variable %s in function %s\n",$1,curr_func);
									exit(1);
								}
								idnode = createNode(entry->type,0,0,$1,NULL,NULL,NULL,NULL);
							}
	            $$ = idnode;
	        }

			| dataField {$$ = $1;}
			;


explist : explist ',' expr {
						struct exprlist *temp = malloc(sizeof(struct exprlist)),*ptr;
						temp->t = $3;
						ptr = ehead[top];
						if(ptr != NULL){
							while(ptr->next != NULL){
								ptr = ptr->next;
							}
						}

						ptr->next = temp;

					}

				| expr {
						struct exprlist *temp = malloc(sizeof(struct exprlist));
						temp->t = $1;
						temp->next = NULL;
						ehead[++top] = temp;
					}

				;


dataField : dataField '.' ID {
																struct tnode *node,*temp;
																struct Fieldlist *fentry = FLookup($1->type,$3);

																if(fentry == NULL){
																	printf("Type %s has no field %s\n",$1->type->name , $3);
																	exit(1);
																}

																node = createNode(fentry->type,24,0,$3,NULL,NULL,NULL,NULL);

																temp = $1;
																while(temp->ptr1 != NULL){
																	temp = temp->ptr1;
																}

																temp->ptr1 = node;
																$$ = $1;
														}
					| ID '.' ID {
								struct tnode *node;
								struct Lsymbol *lentry = LLookup($1);

								if(lentry != NULL){
									struct Fieldlist *fentry = FLookup(lentry->type , $3);
									if(fentry == NULL){
										printf("Type %s has no field %s\n",lentry->type->name , $3);
										exit(1);
									}

									node = createNode(fentry->type,24,0,$3,NULL,NULL,NULL,NULL);
									$$ = createNode(lentry->type,24,0,$1,node,NULL,NULL,NULL);
								}

								else{
									struct Gsymbol *entry = GLookup($1);
									if(entry == NULL){
										printf("Undeclared variable %s in function %s\n",$1,curr_func);
										exit(1);
									}

									struct Fieldlist *fentry = FLookup(entry->type , $3);
									if(fentry == NULL){
										printf("Type %s has no field %s\n",entry->type->name , $3);
										exit(1);
									}
									node = createNode(fentry->type,24,0,$3,NULL,NULL,NULL,NULL);
									$$ = createNode(entry->type,24,0,$1,node,NULL,NULL,NULL);
								}

							}
					;


%%

yyerror(char const *s)
{
    printf("yyerror %s",s);
}


int main(int argc, char *argv[]) {

	regno = -1;
	vhead = NULL;
	lhead = NULL;
	HEAD = NULL;
	thead = NULL;
	fbind = 0;
	numarg = 0;
	labelnum = 0;
	etop = 0;

	typeTableCreate();

	GInstall("main",TLookup("integer"),0,NULL);

	if(argc>1) {
		yyin = fopen(argv[1],"r");
		filename = argv[1];
		strcat(filename,".xsm");
	}

	else{
		filename = strdup("sample.xsm");
		yyin = stdin;
	}

	targetfile = fopen(filename,"w");
	yyparse();

	if(argc > 1)	{
		fclose(yyin);
	}

	fclose(targetfile);
	return 0;
}
