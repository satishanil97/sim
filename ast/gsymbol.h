struct Argument {
	char *name;
	//int type;
	struct Typetable *type;
	struct Argument *next;
};

struct Gsymbol {
  char *name; // Name of the Identifier
  //int type; // TYPE can be 1 INTEGER or 2 BOOLEAN or 3 VOID
	struct Typetable *type;
  int size; // Size field for arrays
  int binding; // Address of the Identifier in Memory
  struct Argument *args;
  struct Gsymbol *next; //Pointer to next symbol table entry
};

struct Gsymbol *HEAD;

//Creates a Global symbol table entry with the given parameters
struct Gsymbol* GInstall(char *name, struct Typetable *type, int size, struct Argument *args);

//Search for a GST entry with the given 'name', if exists, return pointer to GST entry else return NULL.
struct Gsymbol* GLookup(char *name);

//      OFFSET FROM 4096 (FOR MEMORY ALLOCATION)
int memory = 0;

//Search for Argument
struct Argument *ArgLookup(struct Argument *args, char *name);

//for local variables
struct Lsymbol {
	char *name;
	//int type;
	struct Typetable *type;
	int binding;
	int isarg;
	struct Lsymbol *next;
};

struct Lsymbol *LInstall(char *name, struct Typetable *type , int isarg);

struct Lsymbol *LLookup(char *name);

struct Lsymbol *lhead;

struct Typetable{
    char *name;                 //type name
    int size;                   //size of the type
    struct Fieldlist *fields;   //pointer to the head of fields list
    struct Typetable *next;     // pointer to the next type table entry
};

struct Typetable *thead;

struct Fieldlist{
  char *name;              //name of the field
  int fieldIndex;          //the position of the field in the field list
  struct Typetable *type;  //pointer to type table entry of the field's type
  struct Fieldlist *next;  //pointer to the next field
};

void typeTableCreate();		//Function to initialise the type table entries with primitive types (integer,boolean) and special entries(null,void).

struct Typetable* TLookup(char *name); // Search through the type table and return pointer to type table entry of type 'name' .
																			// Returns NULL if entry is not found.

struct Typetable* TInstall(char *name , int size , struct Fieldlist *fields); // Creates a type table entry for the (user defined) type of 'name'
																																							// with given 'fields' and returns the pointer to the type table entry.
																																							// The field list must specify the field index, type and name of each field.
																																							// TInstall returns NULL upon failure.
																																							// This routine is invoked when the compiler sees a type definition in the source program.

struct Fieldlist* FLookup(struct Typetable *type, char *name) ; // Searches for a field of given 'name' in the 'fieldlist' of the given user-defined type
																												 // and returns a pointer to the field entry.
																												 // Returns NULL if the type does not have a field of the name.

int GetSize (struct Typetable * type) ; // Returns the amount of memory words required to store a variable of the given type.

char *curr_func;
char *curr_type;
int findex;

int base; //base pointer
int fbind; //binding for function labels

int numarg;
int numloc;
