%option noyywrap
%{
  #include<stdio.h>
  #include<stdlib.h>
  #include<string.h>

  int gen = -1;

  FILE *fp,*fout;

  char *filename1;
  char *filename2;

  struct lnode{
    char *name;
    int addr;
    struct lnode *next;
  };

  struct lnode *table;

  void LInstall(char *lname , int laddr)  {
    struct lnode *temp;
    temp = malloc(sizeof(struct lnode));
    temp->name = strdup(lname);
    temp->addr = laddr;

    temp->next = table;
    table = temp;
  }

  int LLookup(char *lname){
    struct lnode *temp;
    temp = table;

    while(temp != NULL){
      if(!strcmp(temp->name , lname)){
        return temp->addr;
      }
      temp = temp->next;
    }

    return -1;
  }

  struct fnode {
		char *fname;
		int faddr;
		struct fnode *next;
	};

  struct fnode *ftable = NULL;

	void FInstall(char fname[1000], int faddr) {
		struct fnode *temp;
		temp = malloc(sizeof(struct fnode));
		temp->fname = strdup(fname);
		temp->faddr = faddr;

		if(ftable == NULL) {
			temp->next = NULL;
			ftable = temp;
		}
		else if(ftable != NULL) {
			temp->next = ftable;
			ftable = temp;
		}
	}

	int FLookup(char *fname) {
		struct fnode *temp = ftable;
		while(temp != NULL) {
			if(strcmp(temp->fname, fname)==0) {
				return temp->faddr;
			}
			temp = temp->next;
		}
	}

%}

%x GENERATOR
%x TRANSLATOR

%%

      if(gen == 1)
        BEGIN(GENERATOR);

      if(gen == 0){
        BEGIN(TRANSLATOR);
      }

<GENERATOR>"L"[0-9]+":"[0-9]+"\n" {
                                    int laddr,i,j;
                                    char *rawtext,lname[1000],ipval[1000];

                                    rawtext = strdup(yytext);

                                    for(i = 0;rawtext[i] != ':';i++){
                                      lname[i] = rawtext[i];
                                    }
                                    lname[i] = '\0';
                                    i++;
                                    j = 0;
                                    for(;rawtext[i] != '\n';i++){
                                      ipval[j++] = rawtext[i];
                                    }
                                    ipval[j] = '\0';

                                    laddr = atoi(ipval);
                                    LInstall(lname , laddr);
                                }

<GENERATOR>"F"[0-9]+":"[0-9]+"\n" {
                                   int addr;
                        				   int j = 0,i;
                        				   char *string = strdup(yytext);
                        				   char fname[1000];
                        			     char addrs[1000];
                        				   for(i=0;string[i]!=':';i++) {
                        					fname[j++] = string[i];
                         			           }
                        				   fname[j] = '\0';
                        				   j=0;
                        				   i++;
                        				   for(;string[i]!='\n';i++) {
                        					addrs[j++] = string[i];
                        				   }
                        				   addrs[j] = '\0';
                        			           addr = atoi(addrs);
                        				   FInstall(fname,addr);
                        				   printf("installed %s %d\n",fname,addr);
                        			  }

<GENERATOR>. {}

<TRANSLATOR>"F"[0-9]+":"[0-9]+"\n" {}

<TRANSLATOR>"F"[0-9]+ {
                        if(strcmp(yytext,"F0")==0) {
                  				printf("Found F0. Address is %d\n",FLookup(yytext));
                  			}
                  			fprintf(fout, "%d", FLookup(yytext));
                  		}

<TRANSLATOR>"L"[0-9]+":"[0-9]+"\n" {}

<TRANSLATOR>"L"[0-9]+ {fprintf(fout, "%d",LLookup(yytext));
  		      }

<TRANSLATOR>"\n" {fprintf(fout, "\n");
  		 }

<TRANSLATOR>. {fprintf(fout, "%s",yytext);
  	  }

%%
int main(int argc, char *argv[]) {
	gen = 1;
  filename1 = malloc(sizeof(char *));
  strcpy(filename1 , argv[1]);
  strcat(filename1 , ".xsm");

	fp = fopen(filename1, "r");
	yyin = fp;
	yylex();
	printf("lex done\n");

	struct lnode *temp = table;
  while(temp!=NULL) {
		printf("%s -> %d\n",temp->name,temp->addr);
		temp = temp->next;
	}

  gen = 0;
  filename2 = malloc(sizeof(char *));
  strcpy(filename2

     , argv[1]);
  strcat(filename2 , "f.xsm");

  fout = fopen(filename2, "w");
	fp = fopen(filename1, "r");
	yyin = fp;
	yylex();
	return 0;
}
