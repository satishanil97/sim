struct exprlist{
	struct tnode *t;
	struct exprlist *next;
};

struct exprlist *ehead[100];
int etop;

struct tnode{
	//int type;					//type of variable 	1 for int , 2 for bool , 0 for void
	struct Typetable *type;
	int nodetype;			/*type of node	0 for ID , 1 for number , (2,+) (3,-) (4,*) (5,/) (6,<) (7,<=) (8,==) (9,>=) (10,>)
											(11,!=) (12,&&) (13,||) (14,stmts) (15,read) (16,write) (17,=) (18,cond) (19,while)
											(20,break) (21,continue) (22,body) (23,function invocation) (24,Field) (25,free) (26,alloc) (27,null)*/

	int val;			//value of for the expression tree
	char *name;			//for identifiers or functions
	struct tnode *ptr1,*ptr2,*ptr3;  	//left and right branches
	struct exprlist *arglist;						//for function arguments
	struct Lsymbol *lentry;								//local symbol table
	struct Gsymbol *gentry;									//for global identifiers or functions
}tnode;

//Make an AST node
struct tnode *createNode(struct Typetable *type, int nodetype, int val, char *name, struct tnode *ptr1, struct tnode *ptr2, struct tnode *ptr3, struct exprlist *arglist);
/*Make a leaf tnode and set the value of val field*/
//struct tnode* makeLeafNode(int n, char name);

/*Make a tnode with opertor, left and right branches set*/
//struct tnode* makeOperatorNode(char c,struct tnode *l,struct tnode *r);

/*Make a conditional expression node*/
//struct tnode* makeCondNode(struct tnode *l, struct tnode *m, struct tnode *r);

/*To evaluate an expression tree*/
int get_reg();

int free_reg();

void headerGen(FILE *targetfile);

int codeGen(struct tnode *t, FILE *targetfile);

void exitGen(FILE *targetfile);

int regno = -1;

int ip = 0;

int labelnum = 0;

int start[300],end[300];
int top;

int labelGen() {
	return labelnum++;
}
