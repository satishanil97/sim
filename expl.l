%{
  #include<stdio.h>
  #include "y.tab.h"
%}

%%

[a-z] {
        yylval.character = yytext[0];
        return ID;
      }

[0-9]+  {
          yylval.integer = atoi(yytext);
          return NUM;
        }

"read"  {return READ;}

[=] {return ASGN;}

"write" {return WRITE;}

"+" {return PLUS;}

"-" {return MINUS;}

"*" {return MUL;}

"/" {return DIV;}

"(" return *yytext;
")" return *yytext;
";" return *yytext;
"\n"  return NEWLINE;

[\t" "] {}

. {
    yyerror("Invalid character ");
    printf("%s\n",yytext);
    exit(1);
}

%%
yywrap()
{
  return 1;
}
