%option noyywrap
%{
	#include <stdlib.h>
  #include<string.h>
	#include <stdio.h>
  #define YYSTYPE char*
  #include "y.tab.h"
%}


string [\"][A-Za-z|0-9|' '|!|@|#|$|%|^|&|*|(|)|_|-|+|=|{|}|/|;|?|.|,|<|>]*[\"]


%%

{string} {
            yylval = strdup(yytext);
            return STRING;
          }

"+"	return *yytext;

"\n" return NEWLINE;

[\t" "]  {}
.       { yyerror("Invalid character ");
          printf("%s\n",yytext);
          exit(1);
        }

%%
