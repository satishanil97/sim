%{
	#include<stdio.h>
	#include<stdlib.h>

	
	#include "exptree.h"
	#include "exptree.c"

	extern FILE *yyin;

	int yylex(void);
%}

%union {
	struct tnode* treeptr;
	int ival;
	char character;
}

%type <treeptr> expr
%type <treeptr> program
%token <treeptr> ID
%token <treeptr> NUM
%type <treeptr> stmt statements
%token END IF THEN ELSE ENDIF
%token WRITE ASSIGN READ
%left PLUS MINUS
%left MUL DIV
%left LT GT

%%

program : statements END {
				$$ = $1;
				evaluate($$);
				exit(1);}
	;

statements : statements stmt {$$=makeOperatorNode('S', $1, $2);}
	   | stmt {$$ = $1;}
           ;

stmt : ID ASSIGN expr ';'{
    				if(var[$1->idname - 'a'] == NULL) {
					var[$1->idname - 'a'] = malloc(sizeof(int));
				}
				*var[$1->idname - 'a'] = evaluate($3);			
				$$ = makeOperatorNode('=', $1, $3);	
			 }
     | WRITE '(' expr ')' ';' {
				$$ = makeOperatorNode('W', $3, NULL);
			  }
     | READ '(' ID ')' ';' {
				$$ = makeOperatorNode('R', $3, NULL);
			  }
     | IF expr THEN statements ELSE statements ENDIF ';' {
							$$ = makeCondNode($2, $4, $6);
						     }
     ;

expr : expr PLUS expr {$$ = makeOperatorNode('+', $1, $3);}
     | expr MINUS expr{$$ = makeOperatorNode('-', $1, $3);}
     | expr MUL expr {$$ = makeOperatorNode('*', $1, $3);}
     | expr DIV expr {$$ = makeOperatorNode('/', $1, $3);}
     | expr LT expr {$$ = makeOperatorNode('<', $1, $3);}
     | expr GT expr {$$ = makeOperatorNode('>', $1, $3);}
     | '(' expr ')' {$$ = $2;}
     | NUM {$$ = $1;}
     | ID {
		$$  = $1;
     	  }

    ;

%%

yyerror(char const *s) {
	printf("yyerror: %s",s);
}

int main(int argc, char *argv[]) {
	yyin = fopen(argv[1], "r");
	yyparse();
	fclose(yyin);
	return 0;
}

