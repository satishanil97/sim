 struct tnode {
	int flag;
	int val;
	char *op;
	char idname;
	struct tnode *left, *right, *third;
}tnode;

struct tnode *makeLeafNode(int n, char idname);

struct tnode *makeOperatorNode(char c, struct tnode *l, struct tnode *r);

struct tnode *makeCondNode(struct tnode *l, struct tnode *r, struct tnode *t);

int evaluate(struct tnode *t);

int *var[26];
