%{
	#include<stdio.h>
	#include "y.tab.h"
	#include<stdlib.h>

	int number;
%}

%%

"if" {return IF;}
"then" {return THEN;}
"else" {return ELSE;}
"endif" {return ENDIF;}
"write" {return WRITE;}
"read" {return READ;}
[a-z] {yylval.treeptr = makeLeafNode(0, yytext[0]); return ID;}
[0-9]+ {number = atoi(yytext); yylval.treeptr = makeLeafNode(number,'0'); return NUM;}
"=" {return ASSIGN;}
"+" {return PLUS;}
"-" {return MINUS;}
"*" {return MUL;}
"/" {return DIV;}
"<" {return LT;}
">" {return GT;}
[;] {return *yytext;}
[\t] {}
[()] {return *yytext;}
" " {}
[\n] {return END;}
.    {	printf("%s ",yytext);
	yyerror("unknown character\n"); exit(1);}

%%

int yywrap(void) {
	return 1;
}

