#include<stdio.h>
#include<stdlib.h>

struct tnode* makeLeafNode(int n, char idname) {
	struct tnode *temp;
	temp = malloc(sizeof(struct tnode));
	temp->op = NULL;
	temp->val = n;
	temp->left = NULL;
	temp->right = NULL;
	temp->idname = idname;
	if(idname >= 'a' && idname <= 'z') {
		temp->flag = 2;
	}
	else {
		temp->flag = 0;
	}
	return temp;
}

struct tnode* makeOperatorNode(char c, struct tnode *l, struct tnode *r) {
	struct tnode *temp;
	temp = malloc(sizeof(struct tnode));
	temp->op = malloc(sizeof(char));
	*(temp->op) = c;
	temp->flag = 1;
	temp->left = l;
	temp->right = r;
	return temp;
}

struct tnode* makeCondNode(struct tnode *l, struct tnode *r, struct tnode *t) {
	struct tnode *temp;
	temp = malloc(sizeof(struct tnode));
	temp->flag = 3;
	temp->left = l;
	temp->right = r;
	temp->third = t;
	return temp;
}


int evaluate(struct tnode *t) {
	/*if(t->op == NULL) {
		return t->val;
	}*/

	if(t->flag == 0) {
		return t->val;
	}
	else if(t->flag == 2) {
		return *var[t->idname - 'a'];
	}

	else if (t->flag==1) {
		switch(*(t->op)) {
			case '+' : return evaluate(t->left) + evaluate(t->right);
				   break;
			case '-' : return evaluate(t->left) - evaluate(t->right);
				   break;
			case '*' : return evaluate(t->left) * evaluate(t->right);
				   break;
			case '/' : return evaluate(t->left) / evaluate(t->right);
				   break;
		 	case 'S' : evaluate(t->left);
				   if(t->right!=NULL) {
					   evaluate(t->right);
				   }
				   break;
			case '=' : if(var[t->left->idname - 'a'] == NULL) {
					var[t->left->idname - 'a'] = malloc(sizeof(struct tnode));
				   }
				   *var[t->left->idname - 'a'] = evaluate(t->right);
				   break;
			case 'W' : printf("%d ", evaluate(t->left));
				   break;		 	
			case 'R' : var[t->left->idname - 'a'] = malloc(sizeof(int));
				   scanf("%d",var[t->left->idname - 'a']);
				   break; 
			case '<' : if(evaluate(t->left) < evaluate(t->right)) {
					   return 1;
				   }
				   else {
					   return 0;
				   }
				   break;
			case '>' :   if(evaluate(t->left) > evaluate(t->right)) {
					   return 1;
				   }
				   else {
					   return 0;
				   }
				   break;

		}
	}

	else if(t->flag == 3) {
		if(evaluate(t->left) != 0) {
			evaluate(t->right);
		}
		else {
			evaluate(t->third);
		}
	}

}


