%{
  #include<stdio.h>
  #include<stdlib.h>

  int *var[26];
%}

%union  {
  int integer;
  char character;
};

%token  NEWLINE
%token  ID NUM READ ASGN WRITE PLUS MINUS MUL
%type <integer> expr
%type <integer> NUM
%type <character> ID
%left PLUS MINUS
%left MUL DIV

%%

Program : slist NEWLINE
        ;

slist : slist stmt
      | stmt
      ;

stmt : ID ASGN expr ';'      {
                                if(var[$1 - 'a'] == NULL){
                                  var[$1 - 'a'] = malloc(sizeof(int));
                                }

                                *var[$1 - 'a'] = $3;
                             }

      | READ '(' ID ')' ';'  {
                                if(var[$3-'a'] == NULL){
                                      var[$3 - 'a'] = malloc(sizeof(int));
                                }
                                printf("Enter value : ");
                                scanf("%d",var[$3 - 'a']);
                              }

      | WRITE '(' ID ')' ';' {
                                  if(var[$3 - 'a'] == NULL){
                                      var[$3 - 'a'] = malloc(sizeof(int));
                                      *var[$3 - 'a'] = 0;
                                  }
                                  printf("%d\n",*var[$3 - 'a']);
                              }
      ;


expr : expr PLUS expr  {$$ = $1 + $3;}
    | expr MINUS expr   {$$ = $1 - $3;}
    | expr MUL expr   {$$ = $1 * $3;}
    | expr DIV expr   {$$ = $1 / $3;}
    | '(' expr ')'    {$$ = $2;}
    | NUM           {$$ = $1;}
    | ID {
            if(var[$1 - 'a'] == NULL)
              printf("unassigned variable");

            else
              $$ = *var[$1 - 'a'];
          }
    ;

%%

yyerror(char const *s)
{
    printf("yyerror  %s\n",s);
}

int main()
{
  yyparse();
  return 1;
}
