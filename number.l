%option noyywrap
%{
	#include <stdlib.h>
	#include <stdio.h>
%}

number [0-9]+

%%

{number} {printf("Found : %d\n",atoi(yytext));}
[\n\t]  {}
.       { yyerror("Invalid character ");
          printf("%s\n",yytext);
          exit(1);
        }

%%

yyerror(char const *s)
{
  printf("yyerror %s",s);
}

int main(int argc, char *argv[])
{
  if(argc > 1){
    yyin = fopen(argv[1],"r");
  }

	yylex();
	return 1;
}
